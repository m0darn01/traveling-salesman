# Traveling Salesman Brute Force #

This is a simple program to find the shortest-distance path
of a given set of coordinates, also known as the Traveling Salesman
Problem, using a closest edge insertion heuristic. I have tried to 
optimize it as much as possible. Unsigned 64 bit integers are 
used instead of double precision floats. I have not ported it to Windows yet.


Sample files containing cities are located in Docs.

### What is this repository for? ###

* The author, but it's open to you as well.
* CECS 545 Fall 2015. Traveling Salesman Brute Force
* University of Louisville

### How do I get set up? ###

** Linux **

* Have clang installed
* Have node.js installed

* cd traveling-salesman
* cd lib/nanomsg
* ./configure && make
* cd ../..
* make

* cd traveling-salesman/http/gui
* npm install -d
* node app.js


### How to Run? ###

** Linux **

* ./tsp Docs/RandomX.tsp

* Open a new terminal

* cd http/gui
* node app.js
* open your browser at 127.0.0.1:8080
* click 'Get All Cities'
* click an algorithm to run
* you may need to refresh after you run an algorithm
