CC ?= clang
CFLAGS = -w -O2 -Ofast
INCLUDES = -I./lib/
LIBS = -lm lib/nanomsg/.libs/libnanomsg.a -lpthread -lanl
SRCS = src/main.c src/node.c src/mine.c src/permutation.c src/calculation.c src/file.c src/search.c src/state.c src/thread.c src/comm/nanomsg.c src/comm/api.c src/queue.c

all: tsp


tsp: $(SRCS)
	 $(CC) $(CFLAGS) $(INCLUDES) $(SRCS) $(LIBS) -o tsp

clean:
	rm tsp*
