/**************************************************************
                        2015 Matthew Darnell
**************************************************************/
/**
 * Module dependencies.
 **/
var express = require('express');
var http = require('http');
var path = require('path');
var request = require('request');
var nano = require('nanomsg');

var app = express();



// all environments
app.set('port', process.env.PORT || 8080); //std http port
app.use(express.favicon());
app.set('view engine', 'ejs');
app.use(express.logger('dev'));
app.use(express.json()); // to support JSON-encoded bodies
app.use(express.urlencoded()); // to support URL-encoded bodies
app.use(express.methodOverride());
app.use(app.router);
app.use(express.static(path.join(__dirname, 'public')));

// development only
if ('development' == app.get('env')) {
    app.use(express.errorHandler());
}
console.log('Welcome to Traveling Salesman GUI server!');


/**
 *Create Server and router
 **/

var server = http.createServer(app);
server.listen(app.get('port'), function(err, result) {
    console.log('Express server listening on port ' + app.get('port'));
});

//start socket.io
var io = require('socket.io').listen(server);

/*
 *  Bind Nanomsg
 */

var pair = nano.socket('pair');
var addr = 'ipc:///tmp/pair3.ipc'

pair.connect(addr);

/**
 *routes
 **/


app.get('/', function(req, res) {
    res.render('all');
});

/*
* Nanomsg Routes
*/

io.sockets.on('connection', function(socket){
    socket.emit('greeting', {"hi":"hi"});

    socket.on('request', function(data){
        pair.send(data.requestType);
    });
    
    pair.on('data', function(buf){
        var json = JSON.parse(buf);
        socket.emit(json.resultType, json);
       });

   
});
