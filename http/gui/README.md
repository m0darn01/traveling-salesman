Traveling Salesman GUI
=====================

A web interface for the traveling salesman project written in Node.js with express.

To use
=====================
Download Node.js and install: nodejs.org

Install Packages:

npm install -d


To run
=====================
node app.js
