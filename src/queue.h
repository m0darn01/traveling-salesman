#ifndef _QUEUE_H
#define _QUEUE_H
#include <stdint.h>
#include <string.h>
#include <pthread.h>

#define portable_mutex pthread_mutex_t
#define portable_mutex_unlock pthread_mutex_unlock
#define portable_mutex_lock pthread_mutex_lock
#define portable_mutex_init(x) pthread_mutex_init(x, NULL)

struct queueitem { struct queueitem *next,*prev; };

typedef struct queue
{
	struct queueitem *list;
	pthread_mutex_t mutex;
    char name[31],initflag;
} queue_t;

struct queueitem *queueitem(char *str);
struct queueitem *queuedata(void *data,int32_t datalen);

void free_queueitem(void *itemptr);
void lock_queue(queue_t *queue);
void queue_enqueue(char *name,queue_t *queue,struct queueitem *item);
void *queue_dequeue(queue_t *queue,int32_t offsetflag);


#endif
