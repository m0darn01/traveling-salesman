//
// Created by matthew on 8/28/15.
//

#ifndef PROJECT1_NODE_H
#define PROJECT1_NODE_H

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
//a vertex
struct Node {
    uint64_t index;
    uint64_t x, y;
    int visited;
};

//A weighted Edge
struct Edge{
    struct Node *a, *b;
    uint64_t weight;
};

//A set of vertices and edges
struct Graph{
    struct Node *vertices;
    struct Edge *edges;
    uint64_t numVertices, numEdges;
};

//a route of cities
struct Route {
    uint64_t distance;
    uint64_t *indices;
};

//Extern Variables
extern struct Node* cities;
extern uint64_t count;
extern struct Route HighWaterMark;

//Nodes

//Setup/Interal Operations
int nodecmp(struct Node*one, struct Node *two, int length);
void init_node(struct Node*, uint64_t, uint64_t, uint64_t);
void copy_node(struct Node**, struct Node**);
void swap_nodes(struct Node**, struct Node**);
int hasBeenVisited(struct Node*);
void setVisited(struct Node*);

//Operations
void print_node(FILE*, struct Node*);
void print_node_array(FILE*, struct Node*, uint64_t*);

void read_node(char *buf, struct Node*);
void node_array_to_json(char *buf, struct Node*, uint64_t*);
void push_node_array_to_json(char *buf, struct Node *pHead, uint64_t *len, int generation);

struct Node *FindNodeByIndex(struct Node*, uint64_t*);
struct Node *FindClosest(struct Node *cities, int num, struct Node *base);

struct Node *FindClosestAvailableNodeToEdge(struct Node *cities, int length, struct Edge *edge, struct Node*, uint64_t numNodes);

//Edges
void init_edge(struct Edge *edge, struct Node *a, struct Node *b);
uint64_t get_weight(struct Edge *edge);
struct Node *FindClosestNodeToEdgesInGraph(struct Graph *graph, struct Node *cities, int length, int *indexOfNearestEdge);
void print_edge(FILE *, struct Edge*);
void print_edge_array(FILE *, struct Edge*, int *length);

//Graph
void init_graph(struct Graph *graph, struct Node *vertices, int numVertices, struct Edge *edges, int numEdges);

//Routes
void init_route(struct Route*, struct Node*, uint64_t*);
void create_route(struct Route*, struct Node*, uint64_t*, uint64_t*);
void print_route(FILE*, struct Route*, uint64_t*);
void route_to_json(char *buffer, struct Route* route, uint64_t *length);
#endif //PROJECT1_NODE_H
