//
// Created by matthew on 8/28/15.
//

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <math.h>
#include "calculation.h"

#ifdef _WIN32
#include <inttypes.h>
#endif

#include "calculation.h"
#include "node.h"


int nodecmp(struct Node*one, struct Node *two, int length)
{
    int i;
    for(i=0; i<length; i++){
        if(one[i].index != two[i].index){
            return -1;
        }
    }
    return 0;
}

void init_node(struct Node *dest, uint64_t index, uint64_t x, uint64_t y)
{
    if(dest == NULL){
        dest = (struct Node*)malloc(sizeof(struct Node));
    }
    dest->index = index;
    dest->x = x;
    dest->y = y;
    dest->visited = 0;
}

void print_node(FILE *stream, struct Node *node)
{
    #ifdef _WIN32
    fprintf(stream, "index.(%" PRIu64 ")\tx.(%" PRIu64 ")\ty.(%" PRIu64 ")\n", node->index, node->x, node->y);
    #else
    fprintf(stream, "index.(%lu)\tx.(%lu)\ty.(%lu)\n", node->index, node->x, node->y);
    #endif
}

void print_node_array(FILE *stream, struct Node *pHead, uint64_t *len)
{
    uint64_t i;
    for(i=0; i<*len; i++)
        print_node(stream, &pHead[i]);
    fprintf(stream, "\n\n");
}

void read_node(char *buf, struct Node *node)
{
    char xbuf[numDigits(node->x)+1], ybuf[numDigits(node->y)+1];
    uintcharf(xbuf, node->x);
    uintcharf(ybuf, node->y);
    #ifdef _WIN32
    sprintf(buf, "%s{\"index\":%" PRIu64 ",\"x\":%s,\"y\":%s}", buf, node->index, xbuf, ybuf);
    #else
    sprintf(buf, "%s{\"index\":\"%lu\",\"x\":\"%s\",\"y\":\"%s\"}", buf, node->index, xbuf, ybuf);
    #endif
}

void node_array_to_json(char *buf, struct Node *pHead, uint64_t *len)
{
    uint64_t i;
    strcpy(buf, "{\"resultType\":\"getCities\", \"result\":[");
    char buffer[256];
    for(i=0; i<*len-1; i++){
        strcpy(buffer, "");
        read_node(buffer, &pHead[i]);
        strcat(buffer, ", ");
        strcat(buf, buffer);
    }
    read_node(buf, &pHead[*len-1]);
    strcat(buf, "]}");
}

void push_node_array_to_json(char *buf, struct Node *pHead, uint64_t *len, int generation)
{
    uint64_t i;
    sprintf(buf, "{\"resultType\":\"route\", \"generation\": %d, \"result\":[", generation);
    char buffer[256];
    for(i=0; i<*len-1; i++){
        strcpy(buffer, "");
        read_node(buffer, &pHead[i]);
        strcat(buffer, ", ");
        strcat(buf, buffer);
    }
    strcpy(buffer, "");
    read_node(buffer, &pHead[*len-1]);
    strcat(buf, buffer);
    strcat(buf, "]}");
}

void copy_node(struct Node **dest, struct Node **src)
{
    (*dest)->index = (*src)->index;
    (*dest)->x     = (*src)->x;
    (*dest)->y     = (*src)->y;
    (*dest)->visited = (*src)->visited;
}

void swap_nodes(struct Node **x, struct Node **y)
{
    struct Node temp;
    struct Node *pTemp = &temp;
    init_node(pTemp, (*x)->index, (*x)->x, (*x)->y);
    copy_node(x, y);
    copy_node(y, &pTemp);
}

struct Node *FindNodeByIndex(struct Node *nodes, uint64_t *index)
{
    if(nodes==NULL){
        return NULL;
    }
    int i = 0;
    struct Node *temp = &nodes[0];
    while(temp != NULL){
        if(temp->index == *index){
            return temp;
        }
        temp = &nodes[i++];

    }
    return NULL;
}

struct Node *FindClosest(struct Node *cities, int num, struct Node *base)
{
    if(cities==NULL){
        return NULL;
    }
    int i = 0;
    uint64_t lowestDistance = UINT64_MAX, tempDistance = 0, index = 0;
    struct Node *temp = NULL, *best=NULL;
    for(i=0; i<num; i++){
        if(cities[i].index == base->index){
            continue;
        }
        else if(hasBeenVisited(&cities[i]) == 0){
            continue;
        }
        temp = &cities[i];
        if((tempDistance = distance(temp, base)) < lowestDistance){
            lowestDistance = tempDistance;
            index = temp->index;
            best=temp;
        }
    }
    return best;
}

struct Node *FindClosestAvailableNodeToEdge(struct Node *cities, int length, struct Edge *edge, struct Node* nodesInGraph, uint64_t numNodes)
{
    if(cities == NULL){
        fprintf(stderr, "ERROR: Returning NULL from FindClosestAvailableNodeToEdge()\n");
        return NULL;
    }
    uint64_t Ax, Ay, Bx, By;
    uint64_t shortestDistance = UINT64_MAX, tempDistance = 0;
    Ax = edge->a->x;
    Ay = edge->a->y;
    Bx = edge->b->x;
    By = edge->b->y;
    struct Node *temp = NULL;
    int i;
    int shortestIndex = -1;
    for(i=0; i<length; i++){
        temp = &cities[i];
        if(hasBeenVisited(temp) == 0){ //has been visited, meaning already in graph!
            continue;
        }
        if((tempDistance = PointToLineSegment(temp->x, temp->y, Ax, Ay, Bx, By)) < shortestDistance){
            shortestDistance = tempDistance;
            shortestIndex = i;
        }
    }
    if(shortestIndex > -1){
        return &cities[shortestIndex];
    }
    else{
        return NULL;
    }
}



//returns 0 if it has
int hasBeenVisited(struct Node *node)
{
    return node->visited == 1 ? 0 : -1;
}

void setVisited(struct Node *node)
{
    node->visited = 1;
}

void clearVisited(struct Node *cities, int length)
{
    int i;
    for(i=0; i<length; i++){
        struct Node *temp = &cities[i];
        temp->visited = 0;
    }
}

/*
 *
 *  Edge
 *
 *
*/
void init_edge(struct Edge *edge, struct Node *a, struct Node *b)
{
    if(edge == NULL){
        edge = (struct Edge*)malloc(sizeof(struct Edge));
    }
    edge->a = a;
    edge->b = b;
    edge->weight = distance(a , b);
}
uint64_t get_weight(struct Edge *edge)
{
    return edge->weight;
}
struct Node *FindClosestNodeToEdgesInGraph(struct Graph *graph, struct Node *cities, int length, int *indexOfNearestEdge)
{
    if (cities == NULL){
        return NULL;
    }
    int i;
    struct Node *tempNode = NULL;
    struct Node *closestNode = NULL;
    struct Edge *tempEdge = NULL;
    struct Edge *closestEdge = NULL;
    uint64_t shortestDistance = UINT64_MAX;
    uint64_t tempDistance = 0;
    for(i=0; i < graph->numEdges; i++){
        tempEdge = &graph->edges[i];
        tempNode = FindClosestAvailableNodeToEdge(cities, length, tempEdge, graph->vertices, graph->numVertices);
        if(tempNode == NULL){
            return NULL; //done
        }
        if ( (tempDistance = PointToLineSegment(tempNode->x, tempNode->y, tempEdge->a->x, tempEdge->a->y, tempEdge->b->x, tempEdge->b->y)) < shortestDistance){
            shortestDistance = tempDistance;
            closestNode = tempNode;
            closestEdge = tempEdge;
            *indexOfNearestEdge = i;
        }
    }
    if(closestNode == NULL){
        return NULL;
    }
    setVisited(closestNode);
    return closestNode;
}

void print_edge(FILE *f, struct Edge* edge)
{
    fprintf(f, "Edge: %lu(%lu, %lu)-->%lu(%lu, %lu) --- Distance: %lu\n", edge->a->index, edge->a->x, edge->a->y, edge->b->index, edge->b->x, edge->b->y, edge->weight);
    //fprintf(f, "Edge: %lu\n", edge->a->index);

}
void print_edge_array(FILE *f, struct Edge *edges, int *length)
{
    fprintf(stderr, "in print edge array\n");
    int i;
    for(i=0; i<*length; i++)
    {
        fprintf(stderr, "i=%d\n", i);

        print_edge(f, &edges[i]);
    }
    fprintf(f, "\n");
}

/*
 *
 *  Graph
 *
 *
*/

void init_graph(struct Graph *graph, struct Node *vertices, int numVertices, struct Edge *edges, int numEdges)
{
    if (graph == NULL){
        graph = (struct Graph*)malloc(sizeof(struct Graph));
    }
    graph->numEdges = 0;
    graph->numVertices = 0;

    int i;
    graph->vertices = (struct Node*)malloc(sizeof(struct Node)*numVertices);
    graph->edges = (struct Edge*)malloc(sizeof(struct Edge)*numEdges);
    for(i=0; i<numVertices; i++){
        graph->vertices[i] = vertices[i];
    }
    for(i=0; i<numEdges; i++){
        graph->edges[i] = edges[i];
    }
}
//struct Node* find_node(struct Graph *graph, struct Node *node);



/*
 *
 *  Route
 *
 *
*/

void init_route(struct Route *route, struct Node *nodes, uint64_t *len)
{
    route->indices = (uint64_t*)calloc((size_t)(*len), sizeof(uint64_t));
}

//Create a route from an array of struct Node*
void create_route(struct Route *route, struct Node *nodes, uint64_t *len, uint64_t *hwm)
{
    if(route == NULL){
        fprintf(stderr, "ERROR, Route not initialized!\n");
        return;
    }
    uint64_t i=0;
    route->distance = *hwm;
    for(i=0; i<(*len); i++){
        route->indices[i] = nodes[i].index;
    }
}

void print_route(FILE *stream, struct Route *route, uint64_t *len)
{
    uint64_t i;
    for(i=0; i<*len; i++)
    {
        #ifdef _WIN32
        fprintf(stream, "City[%" PRIu64 "] = %" PRIu64 "\n", i+1, route->indices[i]);
        #else
        fprintf(stream, "City[%lu] = %lu\n", i+1, route->indices[i]);
        #endif
    }
    char buf[numDigits(route->distance)+1];
    uintcharf(buf, route->distance);
    fprintf(stream, "Distance.(%s)\n", buf);
}

void route_to_json(char *buffer, struct Route* route, uint64_t *length)
{
    strcpy(buffer, "{\"orderVisited\":[");
    uint64_t i;
    for(i=0; i<*length-1; i++)
    {
        #ifdef _WIN32
        sprintf(buffer, "%s%" PRIu64 ",\n", buffer, route->indices[i]);
        #else
        sprintf(buffer, "%s%lu,\n", buffer, route->indices[i]);
        #endif
    }
    #ifdef _WIN32
    sprintf(buffer, "%s%" PRIu64 "]", buffer, route->indices[*length-1]);
    #else
    sprintf(buffer, "%s%lu]", buffer, route->indices[*length-1]);
    #endif
    char buf[numDigits(route->distance)+1];
    uintcharf(buf, route->distance);
    sprintf(buffer, "%s, \"distance\": %s}", buffer, buf);
}
