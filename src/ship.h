//
// Created by matthew on 11/15/15.
//

#ifndef TRAVELING_SALESMAN_SHIP_H
#define TRAVELING_SALESMAN_SHIP_H

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
//a BattleShip
struct Ship {
    int length;
    int x, y;
    int orientation; //1=vertical,0=horizontal
    int *hit;
    int sunk;
};



//Extern Variables
extern struct Ship* BattleShips;
extern int BattleShipCount;
extern int BattleShipBoardSize;

//ships

//Setup/Interal Operations
int shipcmp(struct Ship*one, struct Ship *two, int length);
void init_Ship(struct Ship*,int,int,int, int orientation);
void copy_Ship(struct Ship**, struct Ship**);
void swap_Ships(struct Ship**, struct Ship**);

int isHit(struct Ship*, int length, int x, int y);

int hasBeenSunk(struct Ship*);
void setSunk(struct Ship*);
inline void clearSunk(struct Ship *ships, int length);
void reset(struct Ship*, int length);

//Operations
void print_Ship(FILE*, struct Ship*);
void print_Ship_array(FILE*, struct Ship*, uint64_t*);

void read_Ship(char *buf, struct Ship*);
void Ship_array_to_json(char *buf, struct Ship*, uint64_t*);
void push_Ship_array_to_json(char *buf, struct Ship *pHead, int *len, int generation, int boardSize);


#endif //TRAVELING_SALESMAN_SHIP_H
