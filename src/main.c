/*********************************************
 *
 *              Matthew Darnell
 *                CECS 545-01
 *          Project 1 - Fall 2015
 *      Traveling Salesman, brute force
 *
 * ******************************************/
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <math.h>
#include "search.h"

#ifdef _WIN32
#include <inttypes.h>
#include <conio.h>
#include <ctype.h>
#endif

#include "node.h"
#include "state.h"
#include "search.h"
#include "permutation.h"
#include "calculation.h"
#include "file.h"
#include "handler.h"
#include "thread.h"
#include "comm/nanomsg.h"

struct Route HighWaterMark;
struct Node* cities;
uint64_t count;


//Battleship
int BattleShipCount;
int BattleShipBoardSize = 0;

struct Mine* Mines;
int MineCount;
int MineSweeperBoardSize;

int main(int argc, char **argv)
{
    uint64_t index = 0, permutations=0;
    count=0;
    cities = NULL;

    if(argc != 2){
        #ifdef _WIN32
        fprintf(stderr, "Usage: tsp.exe <fileName.tsp>\nPress any key to exit.");
        _getch();
        #else
        fprintf(stderr, "Usage: ./tsp <fileName.tsp>\n");
        #endif
        return -1;
    }
    fprintf(stderr, "Welcome to Traveling Salesman\n2015 Matthew Darnell\n");
    init_threads();
    Mines = getMines(argv[1], &count);
    MineSweeperGeneticAlgorithm(Mines, &count);

    sleep(1);
    return 0;

    //Read the file, fill out an array of struct Node*
    cities = getCities(argv[1], &count);
    if(cities == NULL){
        fprintf(stderr, "Could not get cities from file: \"%s\". Aborting.\n", argv[1]);
        #ifdef _WIN32
        fprintf(stderr, "Press any key to exit.");
        _getch();
        #endif
        return -2;
    }
    //calloc count indices in struct Route HighWaterMark
    init_route(&HighWaterMark, cities, &count);

    //init networking
    strcpy(outBuffer, "");
    strcpy(inBuffer, "");


    //init_threads();

    pthread_join(sendThread, NULL);
    pthread_join(recvThread, NULL);

    //GeneticAlgorithm(cities, &count);
    //ClosestEdgeInsertionSearch(cities, (int)count, 3);
    //Closest Edge
    //greedy(cities, count, 0);

    free(cities);

/*
    while(1){
        printf("Enter a message to pass: ");
        fgets(buf, sizeof(buf), stdin);
        buf[strlen(buf)-1] = 0;
        printf("Sending %s\n", buf);
        push((const char*)buf);
        if(strcmp(buf, "quit")==0){
            break;
        }
        sleep(2);
        fprintf(stderr, "In Buffer: %s\n", inBuffer);
    }
*/
    stop_threads();
    #ifdef _WIN32
    _getch();
    #endif
    return 0;
}
