//
// Created by matthew on 9/20/15.
//

#ifndef TRAVELING_SALESMAN_THREAD_H
#define TRAVELING_SALESMAN_THREAD_H

#include <pthread.h>

extern pthread_t sendThread, recvThread;
extern pthread_mutex_t commLock;
extern char outBuffer[8192], inBuffer[2048];

void init_threads();
void stop_threads();

int push(const char* msg);

#endif //TRAVELING_SALESMAN_THREAD_H
