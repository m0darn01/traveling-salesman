//
// Created by matthew on 8/29/15.
//
#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>

#ifdef _WIN32
#include <inttypes.h>
#endif

#include "file.h"
#include "calculation.h"

//Internal Functions
const char *get_filename_ext(const char *filename)
{
    const char *dot = strrchr(filename, '.');
    if(!dot || dot == filename) return "";
    return dot + 1;
}
//End Internal Functions

//Read the .tsp file, return an array of struct Node*. Remember to free!
struct Node *getCities(char *fileName, uint64_t *len)
{

    uint64_t index=0, coordX=0, coordY=0;
    char x[16], y[16];
    int result = 0;
    char line[2048];


    FILE *pFile = fopen(fileName, "r");
    if(pFile==NULL){
        return NULL;
    }
    if(strcmp(get_filename_ext(fileName), "tsp") != 0){
        fprintf(stderr, "ERROR file must be .tsp!\n");
        return NULL;
    }
    struct Node *cities = (struct Node*)calloc((size_t)1, sizeof(struct Node)*5);
    if(cities == NULL){
        fprintf(stderr, "MEMERROR could not allocate memory for cities\n");
        return NULL;
    }
    int count = 0, max = 5;

    while(fgets(line,2048,pFile)){
        #ifdef _WIN32
        if((result=sscanf (line,"%" PRIu64 " %s %s",&index, x, y)) > 0){
        #else
        if((result=sscanf (line,"%lu %s %s",&index, x, y)) > 0){
        #endif
            if(count >= max){
                max *= 2;
                cities = (struct Node*)realloc(cities, sizeof(struct Node) * max);
            }
            char *pEnd;
            coordX = fuint(strtod(x, &pEnd));
            coordY = fuint(strtod(y, &pEnd));
            init_node(&cities[count], index, coordX, coordY);
            count++;
        }
    }
    fclose(pFile);
    *len = count;
    return cities;
}
/*
struct Ship *getShips(char *fileName, int *len)
{

    int length=0;
    int x=0, y=0;
    int result = 0;
    char line[2048];


    FILE *pFile = fopen(fileName, "r");
    if(pFile==NULL){
        return NULL;
    }
    if(strcmp(get_filename_ext(fileName), "tsp") != 0){
        fprintf(stderr, "ERROR file must be .tsp!\n");
        return NULL;
    }
    struct Ship *ships = (struct Node*)calloc((size_t)1, sizeof(struct Ship)*5);
    if(ships == NULL){
        fprintf(stderr, "MEMERROR could not allocate memory for ships\n");
        return NULL;
    }
    int count = 0, max = 5;
    int first = 0;
    while(fgets(line,16,pFile)){
        if((result=sscanf (line,"%d",&length)) > 0){
            if(count >= max){
                max *= 2;
                ships = (struct Ship*)realloc(ships, sizeof(struct Ship) * max);
            }
            char *pEnd;
            if (first == 0)
            {
                BattleShipBoardSize = length;
                first = 1;
                continue;
            }
            }
            init_Ship(&ships[count], length, x, y, 0);
            count++;
        }
    fclose(pFile);
    *len = count;
    return ships;
}
*/
struct Mine *getMines(char *fileName, int *len)
{
    int length=0;
    int x=0, y=0;
    int result = 0;
    char line[2048];


    FILE *pFile = fopen(fileName, "r");
    if(pFile==NULL){
        return NULL;
    }
    if(strcmp(get_filename_ext(fileName), "tsp") != 0){
        fprintf(stderr, "ERROR file must be .tsp!\n");
        return NULL;
    }
    int count = 0, max = 5;
    int first = 0;
    int numMines = 0;
    fgets(line, 16, pFile);

    result = sscanf(line, "%d %d", &length, &numMines);
    struct Mine *mines = (struct Mine*)malloc(numMines * sizeof(struct Mine));
    int i;
  //  fprintf(stderr, "read %d %d\n", length, numMines);
    for (i = 0; i < numMines; i++)
    {
        init_Mine(&mines[i], 0,0);
        //fprintf(stderr, "init %d\n", i);
    }
    MineSweeperBoardSize = length;
    fclose(pFile);
    *len = numMines;
    return mines;
}