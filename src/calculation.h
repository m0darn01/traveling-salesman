//
// Created by matthew on 8/28/15.
//

#ifndef PROJECT1_CALCULATION_H
#define PROJECT1_CALCULATION_H

#include "node.h"

uint64_t fuint(double);
uint64_t numDigits(uint64_t);
double uintf(uint64_t);
void uintcharf(char*, uint64_t);
uint64_t distance(struct Node*, struct Node*);
void trimUintDigits(uint64_t *x);
uint64_t PointToLineSegment(uint64_t PointX, uint64_t PointY, uint64_t LinePointA_x, uint64_t LinePointA_y, uint64_t LinePointB_x, uint64_t LinePointB_y);

#endif //PROJECT1_CALCULATION_H
