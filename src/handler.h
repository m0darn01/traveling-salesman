//
// Created by matthew on 8/28/15.
//

#ifndef PROJECT1_HANDLER_H
#define PROJECT1_HANDLER_H

#include "node.h"
#include "calculation.h"


//function to calculate the complete distance of a permutation
uint64_t calcRouteDistance(struct Node *permutation, uint64_t *len)
{
    uint64_t retVal = 0;
    int i;
    for(i=0; i<*len; i++){
        if(i==*len-1){
            retVal += distance(&permutation[i], &permutation[0]);
            break;
        }
        else
            retVal += distance(&permutation[i], &permutation[i+1]);
    }
    return retVal;
}

//pointer to calcRouteDistance
uint64_t (*calc)(struct Node *, uint64_t *) = calcRouteDistance;

#endif //PROJECT1_HANDLER_H
