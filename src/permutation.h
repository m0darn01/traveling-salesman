//
// Created by matthew on 8/28/15.
//

#ifndef PROJECT1_PERMUTATION_H
#define PROJECT1_PERMUTATION_H

#include "node.h"

void permute(struct Node* nodes, uint64_t index, uint64_t len, uint64_t *permutations, uint64_t (*funcNewPerm)(struct Node*, uint64_t*));

#endif //PROJECT1_PERMUTATION_H
