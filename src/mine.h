//
// Created by matthew on 11/16/15.
//

#ifndef TRAVELING_SALESMAN_MINE_H
#define TRAVELING_SALESMAN_MINE_H


#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
//a Mine
struct Mine{
    int x, y;
    int hit;
};



//Extern Variables
extern struct Mine* Mines;
extern int MineCount;
extern int MineSweeperBoardSize;

//mines

//Setup/Interal Operations
int minecmp(struct Mine *one, struct Mine *two, int);
void init_Mine(struct Mine*,int,int);
void copy_Mine(struct Mine**, struct Mine**);
void swap_Mine(struct Mine**, struct Mine**);



int isHit(struct Mine*, int x, int y);
int isMine(struct Mine*, int length, int x, int y);

void setHit(struct Mine*);
void clearHit(struct Mine *);
void reset(struct Mine*, int length);

//Operations
void print_Mine(FILE*, struct Mine*);
void print_Mine_array(FILE*, struct Mine*, int*);

void read_Mine(char *buf, struct Mine*);
void Mine_array_to_json(char *buf, struct Mine*, int*);
void push_Mine_array_to_json(char *buf, struct Mine *pHead, int *len, int generation, int boardSize);



#endif //TRAVELING_SALESMAN_MINE_H
