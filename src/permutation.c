//
// Created by matthew on 8/28/15.
//

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>

#include "permutation.h"

static uint64_t lowestDistance = UINT64_MAX;

//get all permutations of an array
void permute(struct Node* nodes, uint64_t index, uint64_t len, uint64_t *permutations, uint64_t (*funcNewPerm)(struct Node*, uint64_t*))
{
    if (len == index){
        (*permutations)++;
        //permutation found
        uint64_t tempDistance;
        if((tempDistance = (*funcNewPerm)(nodes, &len)) < lowestDistance){
            create_route(&HighWaterMark, nodes, &len, &tempDistance);
            lowestDistance = tempDistance;
        }
        return;
    }
    else {
        uint64_t j;
        for(j=index; j < len; j++)
        {
            struct Node *tempX, *tempY;
            tempX = &nodes[index];
            tempY = &nodes[j];
            swap_nodes(&tempX, &tempY);
            permute(nodes, index + 1, len, permutations, (*funcNewPerm));
            swap_nodes(&tempX, &tempY);
        }
    }
}
