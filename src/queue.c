
#include "queue.h"
#include <assert.h>


#define DL_APPEND2(head,add,prev,next)                                                         \
do {                                                                                           \
if (head) {                                                                                  \
(add)->prev = (head)->prev;                                                              \
(head)->prev->next = (add);                                                              \
(head)->prev = (add);                                                                    \
(add)->next = NULL;                                                                      \
} else {                                                                                     \
(head)=(add);                                                                            \
(head)->prev = (head);                                                                   \
(head)->next = NULL;                                                                     \
}                                                                                            \
} while (0)


#define DL_APPEND(head,add)                                                                    \
DL_APPEND2(head,add,prev,next)



#define DL_DELETE2(head,del,prev,next)                                                         \
do {                                                                                           \
assert((del)->prev != NULL);                                                                 \
if ((del)->prev == (del)) {                                                                  \
(head)=NULL;                                                                             \
} else if ((del)==(head)) {                                                                  \
(del)->next->prev = (del)->prev;                                                         \
(head) = (del)->next;                                                                    \
} else {                                                                                     \
(del)->prev->next = (del)->next;                                                         \
if ((del)->next) {                                                                       \
(del)->next->prev = (del)->prev;                                                     \
} else {                                                                                 \
(head)->prev = (del)->prev;                                                          \
}                                                                                        \
}                                                                                            \
} while (0)


#define DL_DELETE(head,del)                                                                    \
DL_DELETE2(head,del,prev,next)

int32_t safecopy(char *dest,char *src,long len)
{
    int32_t i = -1;
    if ( dest != 0 )
        memset(dest,0,len);
    if ( src != 0 )
    {
        for (i=0; i<len&&src[i]!=0; i++)
            dest[i] = src[i];
        if ( i == len )
        {
            printf("safecopy: %s too long %ld\n",src,len);
#ifdef __APPLE__
            //getchar();
#endif
            return(-1);
        }
        dest[i] = 0;
    }
    return(i);
}


struct queueitem *queueitem(char *str)
{
   struct queueitem *item = calloc(1,sizeof(struct queueitem) + strlen(str) + 1);
   strcpy((char *)((long)item + sizeof(struct queueitem)),str);
   return(item);
}

struct queueitem *queuedata(void *data,int32_t datalen)
{
   struct queueitem *item = calloc(1,sizeof(struct queueitem) + datalen);
   memcpy((char *)((long)item + sizeof(struct queueitem)),data,datalen);
   return(item);
}


void free_queueitem(void *itemptr) { free((void*)((long)itemptr - sizeof(struct queueitem))); }

void lock_queue(queue_t *queue)
{
   if ( queue->initflag == 0 )
   {
       portable_mutex_init(&queue->mutex);
       queue->initflag = 1;
   }
    portable_mutex_lock(&queue->mutex);
}

void queue_enqueue(char *name,queue_t *queue,struct queueitem *item)
{
   if ( queue->list == 0 && name != 0 && name[0] != 0 )
       safecopy(queue->name,name,sizeof(queue->name));
   if ( item == 0 )
   {
       printf("FATAL type error: queueing empty value\n");//, getchar();
       return;
   }
   lock_queue(queue);
   DL_APPEND(queue->list,item);
   portable_mutex_unlock(&queue->mutex);
   //printf("name.(%s) append.%p list.%p\n",name,item,queue->list);
}

void *queue_dequeue(queue_t *queue,int32_t offsetflag)
{
   struct queueitem *item = 0;
   lock_queue(queue);
   if ( queue->list != 0 )
   {
       item = queue->list;
       DL_DELETE(queue->list,item);
       //printf("name.(%s) dequeue.%p list.%p\n",queue->name,item,queue->list);
   }
    portable_mutex_unlock(&queue->mutex);
   if ( item != 0 && offsetflag != 0 )
       return((void *)((long)item + sizeof(struct queueitem)));
   else return(item);
}
