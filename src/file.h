//
// Created by matthew on 8/29/15.
//

#ifndef PROJECT1_FILE_H
#define PROJECT1_FILE_H

#include "node.h"
#include "mine.h"

struct Node *getCities(char*, uint64_t*);
struct Mine *getMines(char *, int*);

#endif //PROJECT1_FILE_H
