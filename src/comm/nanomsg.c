//
// Created by matthew on 9/20/15.
//

#include "nanomsg.h"

#include <assert.h>
#include <stdio.h>
#include <time.h>
#include <nanomsg/src/nn.h>
#include <nanomsg/src/pair.h>

static int sock;

int server (const char *url)
{
    int to=3;
    sock = nn_socket (AF_SP, NN_PAIR);
    assert (sock >= 0);
    assert (nn_setsockopt (sock, NN_SOL_SOCKET, NN_RCVTIMEO, &to, sizeof (to)) >= 0);
    assert (nn_bind (sock, url) >= 0);
}
int shutdown_nn()
{
    return nn_shutdown(sock,0);
}
int send(const char *url, const char *msg)
{
    fprintf(stderr, "Sending %s\n", msg);
    int to = 5;
    assert (nn_setsockopt (sock, NN_SOL_SOCKET, NN_RCVTIMEO, &to, sizeof (to)) >= 0);
    int sz_n = strlen (msg);// + 1; // '\0' too
    return nn_send (sock, msg, sz_n, 0);
}

int recv(char **output)
{
    char *buf = NULL;
    int to = 100;
    assert (nn_setsockopt (sock, NN_SOL_SOCKET, NN_RCVTIMEO, &to, sizeof (to)) >= 0);
    int result = nn_recv (sock, &buf, NN_MSG, 0);
    if(result > 0){
        buf[result] = 0;
        *output = (char*)malloc(sizeof(char)*(result+1));
        strcpy(*output, buf);
        nn_freemsg(buf);
    }
    return result;
}