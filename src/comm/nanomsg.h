//
// Created by matthew on 9/20/15.
//

#ifndef TRAVELING_SALESMAN_NANOMSG_H
#define TRAVELING_SALESMAN_NANOMSG_H

int server (const char *url);
int send(const char *url, const char *msg);
int recv(char **output);
int shutdown_nn();
#endif //TRAVELING_SALESMAN_NANOMSG_H
