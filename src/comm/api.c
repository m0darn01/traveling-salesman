//
// Created by matthew on 9/21/15.
//
#include <string.h>
#include "../node.h"
#include "../thread.h"
#include "../search.h"
#include "api.h"

void api(const char* query)
{
    char output[8192];
    strcpy(output, "");
    if(strcmp(query, "ping") == 0){
        strcpy(output, "{\"resultType\":\"pong\"}");
    }
    else if(strcmp(query, "getCities") == 0){
        node_array_to_json(output, cities, &count);
    }
    else if(strcmp(query, "getNumberOfCities") == 0)
    {
        #ifdef _WIN32
        sprintf(output, "%" PRIu64 "", count);
        #else
        sprintf(output, "{\"resultType\":\"numberOfCities\", \"result\":%lu}", count);
        #endif
    }
    else if(strcmp(query, "greedy") == 0){
        strcpy(output, "{\"resultType\": \"Begin Simple Greedy Search\"}");
        greedy(cities, count, 0); //TODO: add rate-limiting
    }
    else if(strcmp(query, "closestEdgeInsertion") == 0){
        strcpy(output, "{\"resultType\": \"Begin Closest Edge Insertion Sort\"}");
        ClosestEdgeInsertionSearch(cities, count, count > 4 ? 3 : 2); //TODO: add rate-limiting
    }
    else if(strcmp(query, "getRoute") == 0){
        if((void*)&HighWaterMark == NULL){
            strcpy(output, "Path not yet found!");
        }
        else{
            uint64_t len = count+1;
            route_to_json(output, &HighWaterMark, &len);
        }
    }
    else if(strcmp(query, "genetic") == 0){
        strcpy(output, "{\"resultType\": \"Begin Genetic Search\"}");
        GeneticAlgorithm(cities, &count);
    }
    else if(strcmp(query, "quit") == 0){
        fprintf(stderr, "RECV QUIT\n");
        stop_threads();
    }
    else{
        strcpy(output, "{\"resultType\": \"invalid api call\"}");
    }

    if(strlen(output) > 0){
        push((const char*)output);
    }
}
