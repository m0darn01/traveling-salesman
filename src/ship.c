//
// Created by matthew on 11/15/15.
//
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <math.h>
#include "calculation.h"

#ifdef _WIN32
#include <inttypes.h>
#endif

#include "calculation.h"
#include "ship.h"


int shipcmp(struct Ship*one, struct Ship *two, int length)
{
    int i;
    for(i=0; i<length; i++){
        if(one[i].length != two[i].length){
            return -1;
        }
    }
    return 0;
}

void init_Ship(struct Ship *dest, int length, int x, int y, int orientation)
{
    if(dest == NULL){
        dest = (struct Ship*)malloc(sizeof(struct Ship));
    }
    dest->length = length;
    dest->x = x;
    dest->y = y;
    dest->orientation = orientation;
    dest->hit = (int *) malloc(sizeof(int) * length);
    memset((dest->hit), 0, length*sizeof(int));
    dest->sunk = 0;
}

void print_Ship(FILE *stream, struct Ship *Ship)
{
    fprintf(stream, "length.(%d)\tx.(%d)\ty.(%d)\torientation.(%s)\n", Ship->length, Ship->x, Ship->y, Ship->orientation==1?"vertical":"horizontal");
}

void print_Ship_array(FILE *stream, struct Ship *pHead, uint64_t *len)
{
    uint64_t i;
    for(i=0; i<*len; i++)
        print_Ship(stream, &pHead[i]);
    fprintf(stream, "\n\n");
}

void read_Ship(char *buf, struct Ship *Ship)
{
    sprintf(buf, "%s{\"len\":%d,\"x\":%d,\"y\":%d, \"orientation\":\"%s\"}", buf, Ship->length, Ship->x, Ship->y, Ship->orientation==1?"vertical":"horizontal");
}

void Ship_array_to_json(char *buf, struct Ship *pHead, uint64_t *len)
{
    uint64_t i;
    strcpy(buf, "{\"resultType\":\"getShips\", \"result\":[");
    char buffer[256];
    for(i=0; i<*len-1; i++){
        strcpy(buffer, "");
        read_Ship(buffer, &pHead[i]);
        strcat(buffer, ", ");
        strcat(buf, buffer);
    }
    read_Ship(buf, &pHead[*len-1]);
    strcat(buf, "]}");
}

void push_Ship_array_to_json(char *buf, struct Ship *pHead, int *len, int generation, int boardSize)
{
    uint64_t i;
    sprintf(buf, "{\"resultType\":\"shipLayout\", \"generation\": %d, \"boardSize\": %d, \"result\":[", boardSize, generation);
    char buffer[256];
    for(i=0; i<*len-1; i++){
        strcpy(buffer, "");
        read_Ship(buffer, &pHead[i]);
        strcat(buffer, ", ");
        strcat(buf, buffer);
    }
    strcpy(buffer, "");
    read_Ship(buffer, &pHead[*len-1]);
    strcat(buf, buffer);
    strcat(buf, "]}");
}

void copy_Ship(struct Ship **dest, struct Ship **src)
{
    (*dest)->length = (*src)->length;
    (*dest)->x     = (*src)->x;
    (*dest)->y     = (*src)->y;
    (*dest)->orientation = (*src)->orientation;
    (*dest)->sunk = (*src)->sunk;
    memcpy(&((*dest)->hit), &((*src)->hit), sizeof(int) * (*src)->length);
}

void swap_Ships(struct Ship **x, struct Ship **y)
{
    struct Ship temp;
    struct Ship *pTemp = &temp;
    init_Ship(pTemp, (*x)->length, (*x)->x, (*x)->y, (*x)->orientation);
    copy_Ship(x, y);
    copy_Ship(y, &pTemp);
}

struct Ship *FindShipByLength(struct Ship *Ships, int *length)
{
    if(Ships==NULL){
        return NULL;
    }
    int i = 0;
    struct Ship *temp = &Ships[0];
    while(temp != NULL){
        if(temp->length == *length){
            return temp;
        }
        temp = &Ships[i++];

    }
    return NULL;
}

//Returns 0 if not hit, 1 if hit, 2 if sunk
int isHit(struct Ship *ships, int length, int x, int y)
{
    int i, j;
    for (i = 0; i < length; i++)
    {
        struct Ship *temp = &ships[i];
        if(hasBeenSunk(temp)==0)
        {
            continue;
        }
        int orientation = temp->orientation;
        int tempX = temp->x;
        int tempY = temp->y;
        int length = temp->length;

        if(orientation==1)//vertical
        {
            if(x == tempX && y >= tempY && y <= tempY + length){
                //fprintf(stderr, "HIT vertical\n");
                temp->hit[y-tempY] = 1;
                int k;
                for(k=0; k<length; k++){
                    if(temp->hit[k] == 0){
                        return 1;
                    }
                }
                setSunk(temp);
                return 2;
            }
        }
        else //horizontal
        {
            if(y == tempY && x >= tempX &&  x <= tempX + length){
                //fprintf(stderr, "HIT horizontal\n");

                temp->hit[x-tempX] = 1;
                int k;
                for(k=0; k<length; k++){
                    if(temp->hit[k] == 0){
                        return 1;
                    }
                }
                setSunk(temp);
                return 2;
            }
        }
    }
    return 0;

}

//returns 0 if it has
int hasBeenSunk(struct Ship *Ship)
{
    return Ship->sunk == 1 ? 0 : -1;
}

void setSunk(struct Ship *Ship)
{
    Ship->sunk = 1;
}

inline void clearSunk(struct Ship *ships, int length)
{
    int i;
    for(i=0; i<length; i++){
        struct Ship *temp = &ships[i];
        temp->sunk = 0;
    }
}

void reset(struct Ship *ships, int length)
{
    clearSunk(ships, length);
    int i, j;
    for (i = 0; i < length; i++)
    {
        struct Ship *temp = &ships[i];
        int *length = &temp->length;
        for (j = 0; j < *length; j++)
        {
            temp->hit[j] = 0;
        }


    }

}

