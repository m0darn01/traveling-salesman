//
// Created by matthew on 9/14/15.
//
#include <stdio.h>
#include <stdlib.h>
#include <float.h>
#include <time.h>
#include "search.h"
#include "state.h"
#include "calculation.h"
#include "thread.h"

void greedy(struct Node *cities, int length, int initialIndex)
{
    clearVisited(cities, length);
    uint64_t routeVisited[length+1];
    uint64_t totalDistance=0;
    int i;
    struct Node *temp = &cities[initialIndex];
    struct Node *next = NULL;
    routeVisited[0] = temp->index;
    for(i=0; i<length-1; i++){
        next = FindClosest(cities, length, temp);
        if(next==NULL){
            fprintf(stderr, "ERROR finding closest node to index.(%lu)\n", temp->index);
            break;
        }
        totalDistance += distance(temp, next);
        char buf[256];
        char x[32],y[32],x2[32],y2[32];
        uintcharf(x, temp->x);
        uintcharf(y, temp->y);
        uintcharf(x2, next->x);
        uintcharf(y2, next->y);
        sprintf(buf, "{\"resultType\":\"step\",\"x1\":%s,\"y1\":%s,\"x2\":%s,\"y2\":%s}",x,y,x2,y2);
        push((const char*)buf);
        sleep(1);
        setVisited(temp);
        temp = next;
        routeVisited[i+1] = temp->index;
    }
    //return to starting city
    next = &cities[initialIndex];
    routeVisited[length] = next->index;
    totalDistance += distance(temp, next);
    char buf[256];
    char x[32],y[32],x2[32],y2[32];
    uintcharf(x, temp->x);
    uintcharf(y, temp->y);
    uintcharf(x2, next->x);
    uintcharf(y2, next->y);
    sprintf(buf, "{\"resultType\":\"step\",\"x1\":%s,\"y1\":%s,\"x2\":%s,\"y2\":%s}",x,y,x2,y2);
    push((const char*)buf);
    sleep(1);
    for(i=0; i<length+1; i++){
        HighWaterMark.indices[i] = routeVisited[i];
    }
    HighWaterMark.distance = totalDistance;
    fprintf(stderr, "\n");
}
#define trace fprintf(stderr, "LINE %d\n", __LINE__);

void PushGraph(struct Graph* graph)
{
    char buf[8192];
    char X[32], Y[32];
    strcpy(buf, "{\"resultType\":\"CEIS\", \"vertices\":[");

    int i;
    for(i=0; i<graph->numVertices-1; i++){
        uintcharf(X, graph->vertices[i].x);
        uintcharf(Y, graph->vertices[i].y);
        sprintf(buf, "%s{\"x\":%s,\"y\":%s},", buf, X, Y);
    }
    uintcharf(X, graph->vertices[graph->numVertices-1].x);
    uintcharf(Y, graph->vertices[graph->numVertices-1].y);
    sprintf(buf, "%s{\"x\":%s,\"y\":%s}],\"edges\":[", buf, X, Y);

    for(i=0; i<graph->numEdges-1; i++){
        uintcharf(X, graph->edges[i].a->x);
        uintcharf(Y, graph->edges[i].a->y);
        sprintf(buf, "%s{\"x1\":%s,\"y1\":%s,", buf, X, Y);
        uintcharf(X, graph->edges[i].b->x);
        uintcharf(Y, graph->edges[i].b->y);
        sprintf(buf, "%s\"x2\":%s,\"y2\":%s},", buf, X, Y);
    }
    uintcharf(X, graph->edges[graph->numEdges-1].a->x);
    uintcharf(Y, graph->edges[graph->numEdges-1].a->y);
    sprintf(buf, "%s{\"x1\":%s,\"y1\":%s,", buf, X, Y);
    uintcharf(X, graph->edges[graph->numEdges-1].b->x);
    uintcharf(Y, graph->edges[graph->numEdges-1].b->y);
    sprintf(buf, "%s\"x2\":%s,\"y2\":%s}]", buf, X, Y);
    strcat(buf, "}");
    push((const char*)buf);
    sleep(1);
}



void ClosestEdgeInsertionSearch(struct Node *cities, int length, int numStartingRandomValues)
{
    clearVisited(cities, length);

    if(length < 2){
        return;
    }
    if(numStartingRandomValues >= length){
        numStartingRandomValues = 1;
    }

    struct Graph * graph = (struct Graph*)malloc(sizeof(struct Graph));
    graph->numEdges = 0;
    graph->numVertices = 0;
    graph->edges =    (struct Edge*)malloc(sizeof(struct Edge)*length);
    graph->vertices = (struct Node*)malloc(sizeof(struct Node)*length);

    //Pick numStartingRandomValues random cities
    time_t t;
    srand((unsigned) time(&t));
    int i;
    struct Node *temp = NULL;
    printf("\n");
    for (i = 0; i < numStartingRandomValues; i++){
        int index = rand() % length;
        while(hasBeenVisited(&cities[index]) == 0){
            index = rand() % length;
        }
        temp = &cities[index];
        setVisited(temp);
        //Add to Graph
        graph->numVertices++;
        struct Node *tempGraph = &graph->vertices[graph->numVertices-1];
        copy_node(&tempGraph, &temp);
    }
    //Draw Edges Around Randomly Selected Nodes
    for(i=0; i<graph->numVertices; i++){
        struct Node* a = &graph->vertices[i];
        struct Node *b = NULL;
        if(i==graph->numVertices-1){
            b = &graph->vertices[0];
        }
        else{
            b = &graph->vertices[i+1];
        }
        graph->numEdges++;
        init_edge(&graph->edges[graph->numEdges-1], a, b);
    }


    //Push First Randomly selected Graph
    PushGraph(graph);

    //At this point (with 3 random cities selected), Graph Contains:
    /*
     *               *
     *             /  |
     *           *----*
     *
     */
    //From now, iteratively find the closest non-visited node to any edge. Add it to the graph, erase the
    //edge and draw two new ones, i.e. a--b --> a--c--b
    struct Node* tempGraphNode = NULL;
    int edgeIndex = -1;
    for(i=0; i < (graph->numVertices); i++){
        edgeIndex = -1;
        //Find Closest Node to all Edges in the Graph, FindClosestNodeToEdgesInGraph also marks it as Visited
        temp = FindClosestNodeToEdgesInGraph(graph, cities, length, &edgeIndex);
        if(temp == NULL){
            break; //we are done
        }
        if(edgeIndex==-1 || edgeIndex > graph->numEdges){
            fprintf(stderr, "ERROR getting the index of the nearest Edge\n");
            break;
        }
        //Add Node to The Graph
        graph->numVertices++;
        tempGraphNode = &graph->vertices[graph->numVertices-1];
        copy_node(&tempGraphNode, &temp);
        //Recombine Edges
        struct Node* a = graph->edges[edgeIndex].a;
        struct Node* b = graph->edges[edgeIndex].b;
        graph->edges[edgeIndex].b = tempGraphNode;
        graph->numEdges++;
        init_edge(&graph->edges[graph->numEdges-1], tempGraphNode, b);

        PushGraph(graph);
    }
    free(graph->edges);
    free(graph->vertices);
    free(graph);
}



/**********************************Genetic Algorithm****************************************************/

int isContained(int *arr, int *value, int *length)
{
    int i;
    for(i=0; i<*length; i++){
        if(arr[i] == *value && arr[i] !=0){
            return 1;
        }
    }
    return 0;
}

int structContains(struct Node *arr, uint64_t *index, int *length)
{
    int i;
    for(i=0; i<*length; i++){
        if(arr[i].index == *index && arr[i].index !=(uint64_t)0){
            return 1;
        }
    }
    return 0;
}
#define trace fprintf(stderr, "%d\n", __LINE__);
void lcs(struct Node *a, struct Node *b, int len, struct Node **out, int *outlen)
{
    struct Node temp[len*2], temp2[len*2];
    memset(temp, 0, sizeof(struct Node)*len*2);
    memset(temp2, 0, sizeof(struct Node)*len*2);

    int count = 0;
    int time=0;
    int numFound = 0;

    int i, j, found = 0, firstFound = 0;
    for(i=0; i<len-1; i++){

        for(j=0; j<len-1; j++){
            found = 0;
            if(b[i].index==a[j].index){

                int offset=j;
                while(j<=len-1 && b[i+1].index == a[j+1].index){
                    i++;
                    j++;
                    found++;
                }

                if(found > 0){
                    int k;
                    time++;
                    for(k=offset; k<j; k++){

                        struct Node *ptr1 = &temp[numFound], *ptr2 = &a[k];

                        if(structContains(&temp[0], &a[k].index, &numFound) == 1){
                           // fprintf(stderr, "SKIPPING %lu\n", a[k].index);
                            break;
                        }
                        copy_node(&ptr1, &ptr2);
                        numFound++;
                    }
                }
            }
        }
    }
   // fprintf(stderr, "after first\n");

    firstFound = numFound;
    numFound = 0;
    found = 0;
    for(i=0; i<len-1; i++){
        for(j=0; j<len-1; j++){
            found = 0;
            if(a[i].index==b[j].index){
                int offset=j;
                while(j<=len-1 && a[i+1].index == b[j+1].index){
                    i++;
                    j++;
                    found++;
                }
                if(found > 0){
                    int k;
                    time++;
                    for(k=offset; k<j; k++){

                        struct Node *ptr3 = &temp2[numFound], *ptr4 = &b[k];
                        if(structContains(&temp2[0], &b[k].index, &numFound) == 1){
                         //   fprintf(stderr, "SKIPPING %lu\n", b[k].index);
                            break;
                        }

                        copy_node(&ptr3, &ptr4);
                        numFound++;
                    }
                }
            }
        }
    }
 //   fprintf(stderr, "out of for loop\n");
    *outlen = firstFound > numFound ? firstFound : numFound;
    uint64_t leng = *outlen;
   // fprintf(stderr, "len=%lu\n", len);
   // printf(stderr, "outlen=%d\n", *outlen);
    if(*outlen==0){
        return;
    }
    *out = (struct Node*)malloc(sizeof(struct Node)*(*outlen));
 //   fprintf(stderr, "after malloc\n");
    if(firstFound > numFound){
        memcpy(*out, temp, *outlen*sizeof(struct Node));
       // fprintf(stderr, "OUT\n");
       // print_node_array(stderr, *out, &leng);

    }
    else{
        memcpy(*out, temp2, *outlen*sizeof(struct Node));
       // fprintf(stderr, "OUT\n");

      //  print_node_array(stderr, *out, &leng);

    }
    if(*outlen==0)
        return;
   // fprintf(stderr, "out[0] = %lu\n", out[0]->index);
}

// Returns the selected index based on the weights(probabilities)
int rouletteSelect(double *weights, int length) {
    // calculate the total weight
    double weight_sum = 0;
    int i;
    for(i=0; i<length; i++) {
        weight_sum += weights[i];
    }
    // get a random value
    double value = ((double) rand() / (RAND_MAX)) * weight_sum;
    // locate the random value based on the weights
    for(i=0; i<length; i++) {
        value -= weights[i];
        if(value <= 0) return i;
    }
    // only when rounding errors occur
    return length - 1;
}


void CreateLinkMatrix(struct Node *route, uint64_t *length, int **out)
{
    //int *o = *out;
    int matrix[*length+1][*length+1];
    memset(&matrix[0][0], 0, sizeof(int)*(*length+1)*(*length+1));
    uint64_t i, j;

        for(j=0; j<= *length-1; j++){


            struct Node *current = &route[j];
            if(current == NULL){
                continue;
            }
            struct Node *next = NULL;
            if (j == *length - 1)
            {
                next = &route[0];
            }
            else{
                next = &route[j+1];
            }
            //fprintf(stderr, "Index.(%lu --> %lu)\n", current->index, next->index);
            matrix[(int)current->index][(int)next->index] += 1;
            //fprintf(stderr, "setting out %d\n", out[(int)current->index][(int)next->index]);
            //out[(int)current->index][(int)next->index] += 1;
        }
    /*
    for (i = 0; i < *length; i++)
    {
        for (j = 0; j < *length; j++)
        {
            if (matrix[i][j] != 0)
            {
                fprintf(stderr, "[%d][%d] = %d ", i, j, matrix[i][j]);
            }
        }
        fprintf(stderr, "\n");
    }
     */
   // *out = (int*)malloc(sizeof(int) * (*length) * (*length));
    memcpy(*out, matrix, sizeof(int) * (*length+1) * (*length+1));
}



void GeneticAlgorithm(struct Node *cities, uint64_t *count)
{
    //1 - pick initial random permutations for the population size
    int populationSize = 60;
    int generations = 100000;
    fprintf(stderr, "Population Size: %d\n", populationSize);

    //population consists of populationSize elements (an element being a path)
    struct Node population[populationSize][*count];
    srand (time(NULL));
    //Get Initial Paths
    uint64_t i;
    int j;
    //iterate over pop. size
    for(i=0; i<populationSize; i++)
    {
        //fill out path for each population

        for (j = 0; j < *count; j++)
        {
            int element = rand() % ((int) (*count)) + 0;     // 0-count

                while (hasBeenVisited(&cities[element]) == 0 && element >=0 && element < *count)
                {
                    element = rand() % ((int) (*count));

                }

            memcpy(&(population[i][j]), &(cities[element]), sizeof(struct Node));
            setVisited(&cities[element]);
        }
        clearVisited(&cities[0], *count);

    }




    //Now, we have our population of randomly selected paths

    struct Node children[populationSize][*count];
    int generation, crossover;


    double mutationRate = 0.015;
    int stuckLocalMin = 0;
    uint64_t shortestDistance = 0;

    uint64_t Distances[populationSize+2];
    uint64_t TotalDistance = 0;

    double probability[*count];
    double sumProbabilities = 0.0;
    char tempDistanceString[256];
    double r = 0.0; //random number
    double dTotalDistance = 0.0, individualDistance = 0.0;
    char buf[256];
    double nextProb = 0.0;
    int par1 = -1, par2=-1;
    int numChildren = 0;
    uint64_t par2Length, par1Length;

    //Iterate over each generation. Basic idea is to do fitness function, generate
    //probability for each, do selection and fill up children array until it has
    //the same populationSize elements as original population, perhaps do a mutation,
    //and replace population array.
    time_t stop, start;
    time(&start);
    double diff = 0.0;
    for(generation=0; generation < generations; generation++){
       // fprintf(stderr, "Generation %d\n", generation);


        /*  Get Distance of Each Path and Total Sum of Distances   */
        TotalDistance = 0;

        for(j=0; j<populationSize; j++){

            Distances[j] = 0;
            for(i=0; i<*count; i++){
                if(i==((*count)-1)){

                    Distances[j] += distance(&(population[j][0]), &(population[j][i]));
                }
                else{

                    Distances[j] += distance(&(population[j][i]), &(population[j][i+1]));

                }

            }

            TotalDistance += Distances[j];
        }

        /*  FITNESS FUNCTION    */

        //Evaluate each of the population according to the fitness function (distance through the path)
        sumProbabilities = 0.0;
        trimUintDigits(&TotalDistance);
        uintcharf(tempDistanceString, TotalDistance);

        dTotalDistance = strtod(tempDistanceString, NULL);
        for(i=0; i<populationSize; i++){
            //Distances are uint64_t, need to ensure 7 decimal place accuracy (trimuint does this)
            //and uintcharf converts to a decimal representation in a string
            trimUintDigits(&Distances[i]);
            uintcharf(buf, Distances[i]);
            individualDistance = strtod(buf, NULL);

            probability[i] = /*sumProbabilities + */(double)((double)1.0-(individualDistance / (dTotalDistance)));
            sumProbabilities += probability[i];

        }
        //fprintf(stderr, "before setting children\n");

        //set children to all 0 (will replace population)
        memset(&children, 0, sizeof(struct Node)*populationSize*(*count));
        numChildren = 0;
        //Iterate until number of children = current population size
        while(numChildren < populationSize){
            nextProb = 0;

            //Pick the two parents for procreation, according to the roulette wheel probability


            par1=-1, par2=-1;
            while(par1==-1){
                par1 = rouletteSelect(probability,populationSize);
            }

            while(par2==-1 && par2 != par1){
                par2 = rouletteSelect(probability,populationSize);
            }


            //Pick my two parents
            struct Node *parentOne = &(population[par1]);
            struct Node *parentTwo = &(population[par2]);
            par1Length = Distances[par1];
            par2Length = Distances[par2];


            //0 <= r <=1
            r = ((double) rand() / (RAND_MAX));
            if(r>0.8){ //Doesn't always crossover!
                if(par1Length>par2Length){
                    memcpy(&children[numChildren][0], &parentTwo[0], sizeof(struct Node) * (*count));
                }
               else{
                    memcpy(&children[numChildren][0], &parentOne[0], sizeof(struct Node) * (*count));
                }
                numChildren++;
                continue;
            }

            //Picking a random number of genes from parent two, add them to the child.
            //Then, iterate through parent 1 and add missing nodes to the child

            uint64_t length = 0;

            struct Node *temp = NULL;
            int outlen = 0;
            lcs(parentOne, parentTwo, *count, &temp, &outlen);

            length = (uint64_t)outlen;
            int GenesFromParentTwo = 0;


            GenesFromParentTwo = *count - length;
            int indicesContained[length];
            memset(&indicesContained, 0, sizeof(int)*(length));

            if(length > 0){
                memcpy(&children[numChildren][0], &temp[0], sizeof(struct Node)*length);

                for(i=0; i<length; i++){
                    indicesContained[i] = temp[i].index;
                }
                free(temp);
            }

            uint64_t nextLength = 0;
            int numInsertions = 0;
            for(i=0; i<*count; i++){
                if (isContained(indicesContained, &parentTwo[i], &length))
                {
                    continue;
                }
                if(numInsertions >= GenesFromParentTwo){
                    break;
                }
                memcpy(&children[numChildren][length+numInsertions], &parentTwo[i], sizeof(struct Node));
                numInsertions++;

            }


            //Only replace with the best of ParentOne, ParentTwo, and the new Child


            uint64_t elShort = 0;
            uint64_t childDist = 0;
            for (i = 0; i < *count; i++)
            {
                if (i == *count - 1)
                {
                    childDist += distance(&(children[numChildren][0]), &(children[numChildren][i]));
                }
                else
                {
                    childDist += distance(&(children[numChildren][i]), &(children[numChildren][i + 1]));
                }
            }

            elShort = par1Length > par2Length ? (par2Length > childDist ? 0 : 1) : ( par1Length > childDist ? 0 : 2 );
            switch(elShort)
            {
                case 0: //Child is best, do nothing
                    break;
                case 1: //Parent 2 is best
                    memcpy(&children[numChildren][0], &parentTwo[0], sizeof(struct Node)*(*count));
                    break;
                case 2: //Parent 1 is best
                    memcpy(&children[numChildren][0], &parentOne[0], sizeof(struct Node)*(*count));
                    break;
                default: //Uh oh
                    break;
            }

            numChildren++;

        } //End crossover

        //Replace Population with Children
        for(i=0; i<populationSize; i++){
            memcpy(&population[i][0], &children[i][0], sizeof(struct Node) * (*count));
        }
        //Mutate?
        for(i=0; i<populationSize; i++)
        {
            for (j = 0; j < (*count); j++)
            {
                r = ((double) rand() / (RAND_MAX));
                r *= 100;
                //0 <= r <= 100
                if (r <= mutationRate)
                {
                    //Mutate!
                    if (j == (*count) - 1)
                    {
                        struct Node temp;
                        memcpy(&temp, &population[i][j], sizeof(struct Node));
                        memcpy(&population[i][j], &population[i][0], sizeof(struct Node));
                        memcpy(&population[i][0], &temp, sizeof(struct Node));
                    }
                    else
                    {
                        struct Node temp;
                        memcpy(&temp, &population[i][j], sizeof(struct Node));
                        memcpy(&population[i][j], &population[i][j + 1], sizeof(struct Node));
                        memcpy(&population[i][j + 1], &temp, sizeof(struct Node));
                    }
                }
            }
        }



        //BEGIN WISDOM OF CROWD



        int matrix[*count+1][*count+1];
        int *ptrMatrix = &matrix;
        //memset(ptrMatrix[0], 0 , *(count+1) * (*count+1));

        for (i = 0; i < *count + 1; i++)
        {
            for (j = 0; j < *count + 1; j++)
            {
                matrix[i][j] = 0;
            }
        }


        struct Node WoCNode[*count+2];
        memset(&WoCNode[0], 0, sizeof(struct Node) * (*count + 2));
        for (i = 0; i < populationSize; i++)
        {
            int out[*count+1][*count+1];
            int *ptrOut = &out;
            memset(&out[0], 0 , (*count+1) * (*count+1));
            struct Node *Population = population[i];

            int k;
            CreateLinkMatrix(Population, count, &ptrOut);

            for (k = 0; k < *count+1; k++)
            {
                for (j = 0; j < *count+1; j++)
                {
                    if (out[k][j] != 0)
                    {
                        //fprintf(stderr, "[%d][%d] = %d \n", k, j, out[k][j]);
                        matrix[k][j] += 1;
                    }
                }
                //fprintf(stderr, "\n");
            }
        }

        int wocContained[*count+1];
        int wocLength = 0;
        memset(&wocContained, 0, sizeof(int)*(*count+1));
        int c =(int)*count;
        c++;
        int nextIndex = 1;
        for (i = 1; i < *count+1; i++)
        {
            //fprintf(stderr, "Population: %d\n", i);
            int max = 0, maxind=0;
            for (j = 1; j < *count + 1; j++)
            {
                if(matrix[i][j] > max){
                    max = matrix[i][j];
                    maxind = j;
                }
                //fprintf(stderr, "%d --> %d : %d\n", i, j, matrix[i][j]);
            }
           // fprintf(stderr, "Max: %d -> %d\n", i, maxind);

            uint64_t maxind64 = (uint64_t)maxind;
            if (isContained(wocContained, &maxind, &c)){
                //fprintf(stderr, "Index.(%lu) is contained, skipping\n", maxind64);
                continue;
            }
            wocContained[i] = maxind64;
            wocLength++;
            struct Node *temp = FindNodeByIndex(population[0], &maxind64);
            struct Node *ptrWoC = &WoCNode[nextIndex-1];
            copy_node(&ptrWoC, &temp);
            nextIndex++;
        }
        //fprintf(stderr, "\n\nWoCNode:\n");

        //print_node_array(stderr, &WoCNode, count);
        //Have aggregated wisdom of crowd node, do simple greedy search to fill in missing nodes

            for (i = 1; i < *count+1; i++)
            {
                if(isContained(wocContained,&i, &c))
                {
                   // fprintf(stderr, "%d contained, skipping\n", i);
                    continue;
                }
                wocContained[nextIndex] = i;
                struct Node *ptrWoC = &WoCNode[nextIndex-1];
                struct Node *temp = FindNodeByIndex(population[0], (uint64_t*)&i);

                copy_node(&ptrWoC, &temp);
                //fprintf(stderr, "Replacing index.(%lu) to woc[%d]\n", temp->index, nextIndex-1);
                nextIndex++;
            }


        //fprintf(stderr, "\n\nWoCNode:\n----------------\n");

       // print_node_array(stderr, &WoCNode, count);

        //fprintf(stderr, "\n-----------------------------------\n");

        //Copy WoC Node to longest route in population -- IF it is shorter
        uint64_t longestGenDistance = 0;
        int elLongest = 0;


        for (j = 0; j < populationSize; j++)
        {

            uint64_t tempDistance = 0;
            for (i = 0; i < *count; i++)
            {

                if (i == *count - 1)
                {
                    tempDistance += distance(&(population[j][0]), &(population[j][i]));
                }
                else
                {
                    tempDistance += distance(&(population[j][i]), &(population[j][i + 1]));
                }
            }
            if (tempDistance > longestGenDistance)
            {
                longestGenDistance = tempDistance;
                elLongest = j;
            }
        }




        uint64_t WoCDistance = 0;


        for (i = 0; i < *count; i++)
        {

            if (i == *count - 1)
            {
                WoCDistance += distance(&(WoCNode[0]), &(WoCNode[i]));
            }
            else
            {
                WoCDistance += distance(&(WoCNode[i]), &(WoCNode[i + 1]));
            }
        }
        if(WoCDistance < longestGenDistance)
        {
            //fprintf(stderr, "Replacing WoC.(%lu) with index %d.(%lu)\n", WoCDistance, elLongest, longestGenDistance);
            memcpy(&population[elLongest][0], &WoCNode, sizeof(struct Node)*(*count));
        }

        
        //END WISDOM OF CROWD




        //After crossing over each generation, pick the best and send to GUI
        if(generation % 5000 == 0)
        {
            uint64_t shortestGenDistance = UINT64_MAX;
            int elShortest = 0;

            for (j = 0; j < populationSize; j++)
            {

                uint64_t tempDistance = 0;
                for (i = 0; i < *count; i++)
                {

                    if (i == *count - 1)
                    {
                        tempDistance += distance(&(population[j][0]), &(population[j][i]));
                    }
                    else
                    {
                        tempDistance += distance(&(population[j][i]), &(population[j][i + 1]));
                    }
                }
                if (tempDistance < shortestGenDistance)
                {
                    shortestGenDistance = tempDistance;
                    elShortest = j;
                }
            }


            char buffer[9000];
            time(&stop);
            char dist[32];
            uintcharf(dist, shortestGenDistance);
            diff = difftime(stop, start);
            time(&start);
            printf("Generation(%d): (Distance = %s)    %f GEN/s\n", generation, dist, 1000/diff);
            if(shortestGenDistance == shortestDistance){
                stuckLocalMin++;

                mutationRate = (double)(stuckLocalMin)*.1;
                //fprintf(stderr, "Local Min! Mutation Rate = %f\n", mutationRate);
            }
            else{

                stuckLocalMin = 0;
                mutationRate = 0.015;
            }
            shortestDistance = shortestGenDistance;
            push_node_array_to_json(buffer, &population[elShortest][0], count, generation);
            push(buffer);
        }



    } //End Generations
} //End algorithm



/*Minesweeper*/

//Function to determine if the placement of a mine overlaps an existing mines' coordinates

int isValidPlacement(struct Mine *mines, int numMines, int x, int y)
{
    int i;
    for (i = 0; i < numMines; i++)
    {
        if (x == mines[i].x && y == mines[i].y)
        {
            return 0; //Already placed
        }
    }
    return 1;
}


unsigned int rand_interval(unsigned int min, unsigned int max)
{
    int r;
    const unsigned int range = 1 + max - min;
    const unsigned int buckets = RAND_MAX / range;
    const unsigned int limit = buckets * range;

    /* Create equal size buckets all in a row, then fire randomly towards
     * the buckets until you land in one of them. All buckets are equally
     * likely. If you land off the end of the line of buckets, try again. */

    do
    {
        r = rand();
    } while (r >= limit);

    return min + (r / buckets);
}

//Return number of adjacent mines. set direction ints to 1 if it's a mine
int getNumAdjacentMines(struct Mine *mines, int startX, int startY, int numMines, int boardSize)
{
   // fprintf(stderr, "get num adjacent mines - %d %d\n", startX,startY);
    if(isMine(mines, numMines, startX, startY))
    {
        return 20;
    }
   // fprintf(stderr, "not mine\n");
    if(startX > boardSize-1 || startY > boardSize-1 || startX < 0 || startY < 0)
    {
      //  fprintf(stderr, "return 20\n");
        return 20;
    }
    //fprintf(stderr, "%d %d\n", startX, startY);
    int i;

    int count = 0;
    //squares to check
    int left=1, right=1, up=1, down=1;

    //Corners
    if(startX == boardSize-1 && startY == boardSize-1) //top right
    {
        up=0;
        right=0;
    }
    else if(startX == 0 && startY == 0) //bottom left
    {
        left=0;
        down=0;
    }
    else if(startX == 0 && startY == boardSize-1) //top left
    {
        left=0;
        up=0;
    }
    else if(startX == boardSize-1 && startY == 0) //bottom right
    {
        right=0;
        down=0;
    }


    if (left == 1)
    {
        if (isMine(mines, numMines, startX - 1, startY))
        {
            count++;
        }
    }
    if (right == 1)
    {
        if (isMine(mines, numMines, startX + 1, startY))
        {
            count++;
        }
    }
    if (down == 1)
    {
        if (isMine(mines, numMines, startX, startY - 1))
        {
            count++;
        }
    }
    if (up == 1)
    {
        if (isMine(mines, numMines, startX, startY + 1))
        {
            count++;
        }
    }
    //fprintf(stderr, "gonna return count\n");

    return count;
}

void PrintBoard(int *Board[MineSweeperBoardSize + 1], int boardSize)
{
    int i, j;
    fprintf(stderr, "\n\t0");

    for (j = 1; j < boardSize; j++)
    {
        fprintf(stderr, "      %d", j);
    }

    fprintf(stderr, "\n");
    for (i = 0; i < boardSize; i++)
    {
        fprintf(stderr, "%d -- ", i);
        for (j = 0; j < boardSize; j++)
        {
            if (Board[i][j] != 0)
            {
                fprintf(stderr, " | %d | ", Board[i][j]);
            }
            else
            {
                fprintf(stderr, " |   | ");
            }
        }
        fprintf(stderr, "\n");
    }
}

int FillAdjacentUpLeftEmpty(int *Board[MineSweeperBoardSize], struct Mine *mines, int x, int y, int numMines, int boardSize)
{
    int tempX = x;
    int tempY = y;
    int num = 0;
    if (x < 0 || y < 0)
    {
        return -1;
    }
    if ((num = getNumAdjacentMines(mines, tempX, tempY, numMines, boardSize) == 0))
    {
        if (Board[tempX][tempY] == 0)
        {
            Board[tempX][tempY] = 5;
        }

        FillAdjacentUpLeftEmpty(Board, mines, tempX - 1, tempY, numMines, boardSize);
        FillAdjacentUpLeftEmpty(Board, mines, tempX, tempY - 1, numMines, boardSize);
    }
    return 0;
}

int FillAdjacentUpRightEmpty(int *Board[MineSweeperBoardSize], struct Mine *mines, int x, int y, int numMines, int boardSize)
{
    int tempX = x;
    int tempY = y;
    int num = 0;
    if (x < 0 || y < 0 || x > boardSize-1)
    {
        return -1;
    }
    if ((num = getNumAdjacentMines(mines, tempX, tempY, numMines, boardSize) == 0))
    {
        if (Board[tempX][tempY] == 0)
        {
            Board[tempX][tempY] = 5;
        }

        FillAdjacentUpRightEmpty(Board, mines, tempX + 1, tempY, numMines, boardSize);
        FillAdjacentUpRightEmpty(Board, mines, tempX, tempY - 1, numMines, boardSize);
    }
    return 0;
}

int FillAdjacentDownRightEmpty(int *Board[MineSweeperBoardSize], struct Mine *mines, int x, int y, int numMines, int boardSize)
{
    int tempX = x;
    int tempY = y;
    int num = 0;
    if (x > boardSize-1 || y > boardSize-1)
    {
        return -1;
    }
    if ((num = getNumAdjacentMines(mines, tempX, tempY, numMines, boardSize) == 0))
    {
        if (Board[tempX][tempY] == 0)
        {
            Board[tempX][tempY] = 5;
        }

        FillAdjacentDownRightEmpty(Board, mines, tempX + 1, tempY, numMines, boardSize);
        FillAdjacentDownRightEmpty(Board, mines, tempX, tempY + 1, numMines, boardSize);
    }
    return 0;
}

int FillAdjacentDownLeftEmpty(int *Board[MineSweeperBoardSize], struct Mine *mines, int x, int y, int numMines, int boardSize)
{
    int tempX = x;
    int tempY = y;
    int num = 0;
    if (x > boardSize-1 || y > boardSize-1)
    {
        return -1;
    }
    if ((num = getNumAdjacentMines(mines, tempX, tempY, numMines, boardSize) == 0))
    {
        if (Board[tempX][tempY] == 0)
        {
            Board[tempX][tempY] = 5;
        }

        FillAdjacentDownLeftEmpty(Board, mines, tempX - 1, tempY, numMines, boardSize);
        FillAdjacentDownLeftEmpty(Board, mines, tempX, tempY + 1, numMines, boardSize);
    }
    return 0;
}

int FillAdjacentLeftEmpty(int *Board[MineSweeperBoardSize], struct Mine *mines, int x, int y, int numMines, int boardSize)
{
    int tempX = x;
    int tempY = y;
    int num = 0;
    if (x > boardSize-1 || y > boardSize-1)
    {
        return -1;
    }
    if ((num = getNumAdjacentMines(mines, tempX, tempY, numMines, boardSize) == 0))
    {
        if (Board[tempX][tempY] == 0)
        {
            Board[tempX][tempY] = 5;
        }

        FillAdjacentLeftEmpty(Board, mines, tempX - 1, tempY, numMines, boardSize);
    }
    return 0;
}

int FillAdjacentRightEmpty(int *Board[MineSweeperBoardSize], struct Mine *mines, int x, int y, int numMines, int boardSize)
{
    int tempX = x;
    int tempY = y;
    int num = 0;
    if (x > boardSize-1 || y > boardSize-1)
    {
        return -1;
    }
    if ((num = getNumAdjacentMines(mines, tempX, tempY, numMines, boardSize) == 0))
    {
        if (Board[tempX][tempY] == 0)
        {
            Board[tempX][tempY] = 5;
        }

        FillAdjacentRightEmpty(Board, mines, tempX + 1, tempY, numMines, boardSize);
    }
    return 0;
}

int FillAdjacentUpEmpty(int *Board[MineSweeperBoardSize], struct Mine *mines, int x, int y, int numMines, int boardSize)
{
    int tempX = x;
    int tempY = y;
    int num = 0;
    if (x > boardSize-1 || y > boardSize-1)
    {
        return -1;
    }
    if ((num = getNumAdjacentMines(mines, tempX, tempY, numMines, boardSize) == 0))
    {
        if (Board[tempX][tempY] == 0)
        {
            Board[tempX][tempY] = 5;
        }

        FillAdjacentUpEmpty(Board, mines, tempX, tempY+1, numMines, boardSize);
    }
    return 0;
}

int FillAdjacentDownEmpty(int *Board[MineSweeperBoardSize], struct Mine *mines, int x, int y, int numMines, int boardSize)
{
    int tempX = x;
    int tempY = y;
    int num = 0;
    if (x > boardSize-1 || y > boardSize-1)
    {
        return -1;
    }
    if ((num = getNumAdjacentMines(mines, tempX, tempY, numMines, boardSize) == 0))
    {
        if (Board[tempX][tempY] == 0)
        {
            Board[tempX][tempY] = 5;
        }

        FillAdjacentDownEmpty(Board, mines, tempX, tempY-1, numMines, boardSize);
    }
    return 0;
}
int FillAdjacentEmpty(int *Board[MineSweeperBoardSize], struct Mine *mines, int x, int y, int numMines, int boardSize)
{

    int tempX = x;
    int tempY = y;
    int num = 0;
    if (x < 0 || y > boardSize)
    {
        return -1;
    }
    //fprintf(stderr, "In Fill Adjacent (%d,%d)\n",x,y);

    if ((num = getNumAdjacentMines(mines, tempX, tempY, numMines, boardSize) == 0))
    {
        //fprintf(stderr, "Board[%d][%d]=5\n", tempX, tempY);
        if (Board[tempX][tempY] == 0)
        {
            Board[tempX][tempY] = 5;
        }
    }
        FillAdjacentUpLeftEmpty(Board, mines, tempX - 1, tempY - 1, numMines, boardSize);
        FillAdjacentDownRightEmpty(Board, mines, tempX + 1, tempY + 1, numMines, boardSize);
        FillAdjacentUpRightEmpty(Board, mines, tempX - 1, tempY + 1, numMines, boardSize);
        FillAdjacentDownLeftEmpty(Board, mines, tempX + 1, tempY - 1, numMines, boardSize);

        FillAdjacentLeftEmpty(Board, mines, tempX + 1, tempY, numMines, boardSize);
        FillAdjacentRightEmpty(Board, mines, tempX + 1, tempY, numMines, boardSize);
        FillAdjacentDownEmpty(Board, mines, tempX, tempY - 1, numMines, boardSize);
        FillAdjacentUpEmpty(Board, mines, tempX, tempY + 1, numMines, boardSize);

   // }
    return 0;
}




int playMineSweeper(struct Mine *mines, int numMines, int boardSize)
{
    int **Board;
    Board = malloc(sizeof(int *) * (boardSize + 1));
    int ClearedSpots[boardSize + 1][boardSize + 1];
    memset(&ClearedSpots[0][0], 0, sizeof(int) * (boardSize + 1) * (boardSize + 1));
    int firstFlag = 0;

    //int Board[boardSize+1][boardSize+1];
    int i, j;
    for (i = 0; i < boardSize; i++)
    {
        Board[i] = (int *) malloc(sizeof(int) * (boardSize + 1));
    }
    memset(&Board[0][0], 0, sizeof(int) * (boardSize + 1) * (boardSize + 1));
    for (i = 0; i < numMines; i++)
    {
        struct Mine *temp = &mines[i];
        Board[temp->x][temp->y] = 2;
    }

    int playX, playY;
    int numMoves = 0;
    int hitMine = 0;


    int minX = boardSize - 1, minY = boardSize - 1;
    int minAdjacent = numMines, num;
    playX = rand_interval(0, boardSize - 1);
    playY = rand_interval(0, boardSize - 1);
    while (hitMine == 0)
    {
        minX = boardSize - 1, minY = boardSize - 1;
        minAdjacent = numMines;
        for (i = 1; i < boardSize; i++)
        {
            for (j = 1; j < boardSize; j++)
            {
                if (Board[i][j] != 5 && Board[i][j] != 1)
                {
                    continue;
                }
                // fprintf(stderr, "getadj\n");
                num = getNumAdjacentMines(mines, i, j, numMines, boardSize - 1);
                //   fprintf(stderr, "num=%d min=%d cleared=%d\n", num, minAdjacent, ClearedSpots[i][j] == 0);
                if (num < minAdjacent && Board[i][j] == 5 || Board[i][j] == 1)
                {
                    minX = i;
                    minY = j;
                    //  fprintf(stderr, "MinX=%d MinY=%d\n", minX, minY);
                    minAdjacent = num;
                }


            }
        }


//         fprintf(stderr, "minx=%d miny=%d\n", minX, minY);
        //Got node with minimum known adjacent mines
        //Now, pick an open node next to it

        if (minX <= 0 && minY <= 0)
        {
            if (Board[minX + 1][minY + 1] == 0)
            {
                playX = minX + 1;
                playY = minY + 1;
            }
            else if ((Board[minX + 1][minY] == 0))
            {
                playX = minX + 1;
                playY = minY;
            }
            else if ((Board[minX][minY + 1] == 0))
            {
                playX = minX;
                playY = minY + 1;
            }
            else
            {
                playX = rand_interval(0, boardSize - 1);
                playY = rand_interval(0, boardSize - 1);
            }
        }
        else if (minX <= 0 && minY >= boardSize - 1)
        {
            if (Board[minX + 1][minY - 1] == 0)
            {
                playX = minX + 1;
                playY = minY - 1;
            }
            else if ((Board[minX + 1][minY] == 0))
            {
                playX = minX + 1;
                playY = minY;
            }
            else
            {
                playX = rand_interval(0, boardSize - 1);
                playY = rand_interval(0, boardSize - 1);
            }
        }
        else if (minX >= boardSize - 1 && minY <= 0)
        {
            if (Board[minX - 1][minY + 1] == 0)
            {
                playX = minX - 1;
                playY = minY + 1;
            }
            else if ((Board[minX - 1][minY] == 0))
            {
                playX = minX - 1;
                playY = minY;
            }
            else if ((Board[minX][minY + 1] == 0))
            {
                playX = minX;
                playY = minY + 1;
            }
            else
            {
                playX = rand_interval(0, boardSize - 1);
                playY = rand_interval(0, boardSize - 1);
            }
        }
        else if (minX >= boardSize - 1 && minY >= boardSize - 1)
        {
            //fprintf(stderr, "minx=%d miny=%d\n", minX, minY);
            if (Board[minX - 1][minY - 1] == 0)
            {
              //  fprintf(stderr, "1\n");
                playX = minX - 1;
                playY = minY - 1;
            }
            else if ((Board[minX][minY] == 0))
            {
              //  fprintf(stderr, "2\n");

                playX = minX;
                playY = minY;
            }
            else
            {
              //  fprintf(stderr, "3\n");

                playX = rand_interval(0, boardSize - 1);
                playY = rand_interval(0, boardSize - 1);
            }
        }


        int num1, num2, num3, num4;
        num1 = getNumAdjacentMines(mines, minX + 1, minY + 1, numMines, boardSize - 1);
        num2 = getNumAdjacentMines(mines, minX - 1, minY - 1, numMines, boardSize - 1);
        num3 = getNumAdjacentMines(mines, minX + 1, minY - 1, numMines, boardSize - 1);
        num4 = getNumAdjacentMines(mines, minX - 1, minY + 1, numMines, boardSize - 1);

        int min = num1;
        playX = minX + 1;
        playY = minY + 1;

        playX = playX > boardSize-1 ? boardSize-1 : playX;
        playY = playY > boardSize-1 ? boardSize-1 : playY;
       // fprintf(stderr, "playX=%d playY=%d\n", playX, playY);

        if (num2 < min || Board[playX][playY] == 5 || Board[playX][playY] == 1)
        {

            min = num2;
            playX = minX - 1;
            playY = minY - 1;
            playX = playX > boardSize-1 ? boardSize-1 : playX;
            playY = playY > boardSize-1 ? boardSize-1 : playY;
        }
        if (num3 < min || Board[playX][playY] == 5 || Board[playX][playY] == 1)
        {
            min = num3;
            playX = minX + 1;
            playY = minY - 1;
            playX = playX > boardSize-1 ? boardSize-1 : playX;
            playY = playY > boardSize-1 ? boardSize-1 : playY;
        }
        if (num4 < min || Board[playX][playY] == 5 || Board[playX][playY] == 1)
        {
            min = num4;
            playX = minX - 1;
            playY = minY + 1;
            playX = playX > boardSize-1 ? boardSize-1 : playX;
            playY = playY > boardSize-1 ? boardSize-1 : playY;
        }

       // fprintf(stderr, "playX=%d playY=%d\n", playX, playY);

        while (Board[playX][playY] == 5 || Board[playX][playY] == 1)
        {
            // fprintf(stderr, "Getting Random\n");
            playX = rand_interval(0, boardSize - 1);
            playY = rand_interval(0, boardSize - 1);
        }


        if (ClearedSpots[playX][playY] != 0 || Board[playX][playY] == 5)
        {
            //fprintf(stderr, "%d,%d already played\n", playX, playY);
            continue;
        }
      // fprintf(stderr, "PLAYING %d,%d\n", playX, playY);
        numMoves++;

        if (firstFlag == 0)
        {
            playX = rand_interval(0, boardSize - 2);
            playY = rand_interval(0, boardSize - 2);
            firstFlag = 1;
        }
       //  fprintf(stderr, "Playing (%d,%d)\n", playX, playY);

        ClearedSpots[playX][playY] = 1;


        if (isMine(mines, numMines, playX, playY)) //hit a mine, game over.
        {
           // fprintf(stderr, "HIT (%d,%d)\n", playX, playY);
            hitMine = 1;
            break;
        }
        int num = 0;
        int tempX = playX;
        int tempY = playY;
        // fprintf(stderr, "before filladjacent\n");
        FillAdjacentEmpty(Board, mines, playX, playY, numMines, boardSize);
        //  fprintf(stderr, "after filladjacent\n");

        Board[playX][playY] = 1;
    }
    //PrintBoard(Board, boardSize);

    free(Board);
    return numMoves;
}

double GetAverageMineSweeperPlays(struct Mine *population, int *numMines, int boardSize)
{
    int NUM_PLAYS = 200;
    int sum=0, i;
    double avg = 0.0;
    for (i = 0; i < NUM_PLAYS; i++)
    {
        sum += playMineSweeper(&population[0], *numMines, boardSize);
    }
    avg = (double)sum / NUM_PLAYS;
    //fprintf(stderr, "AVG.(%f) = %d / %d\n", avg, sum, NUM_PLAYS);
    return avg;
}


//Get common mines for crossover
int GetCommonMines(struct Mine *child, struct Mine *parent1, struct Mine *parent2, int length)
{
    int numFound = 0, i,j;
    for (i = 0; i < length; i++)
    {
        struct Mine *temp = &(parent1[i]);
        for (j = 0; j < length; j++)
        {
            if (i == j){
                continue;
            }
            struct Mine *par2Temp = &parent2[j];

            if(par2Temp->x == temp->x && par2Temp->y == temp->y)
            {
                //common mine!
                struct Mine *tempChild = &child[numFound];
                copy_Mine(&tempChild, &temp);
                numFound++;
            }
        }
    }
    return numFound;
}


int isMineContained(struct Mine *mines, struct Mine *mine, int length)
{
    int i;
    for (i = 0; i < length; i++)
    {
        struct Mine *temp = &mines[i];
        if(temp->x == mine->x && temp->y == mine->y){
            return 1;
        }
    }
    return(0);
}

//The second part of a crossover. First, cpy all common mines to a child, then, fill in the unused mines from a parent into the child
//probably the harder parent
void FillInUnusedMines(struct Mine *child, struct Mine *parent, int numCommon, int length)
{
    int i,j;
    int numChildren = numCommon;
    for(i=numCommon; i<length; i++)
    {
        struct Mine *childTemp = &child[i];
        for(j=0; j<length; j++){
            struct Mine *parentTemp = &parent[j];
            if(isMineContained(child, parentTemp, numChildren) == 0){
                //fprintf(stderr, "copying parent[%d]\n",j);
                //print_Mine(stderr, parentTemp);

                memcpy(childTemp, parentTemp, sizeof(struct Mine));
                //fprintf(stderr, "to \n");

                //print_Mine(stderr, childTemp);
                numChildren++;
            }
        }
    }

}


//MineSweeper Genetic Algorithm
void MineSweeperGeneticAlgorithm(struct Mine *mines, int *count)
{
    //1 - pick initial random permutations for the population size
    int populationSize = 5;
    int generations = 1;
    int NUM_GAMES = 20;
    fprintf(stderr, "Population Size: %d\n", populationSize);

    //population consists of populationSize elements (an element being a board configuration)
    struct Mine population[populationSize][*count];
    memset(&population[0][0], 0, sizeof(struct Mine) * populationSize * (*count));
    srand(time(NULL));

    /*  GENERATE INITIAL RANDOM POPULATION */
    //Get Initial Randomized Board Placements
    char buffer[9000];
    int i, j;
    //iterate over pop. size
    for (i = 0; i < populationSize; i++)
    {
        //fill out board configuration for each population

        //iterate over each mine
        for (j = 0; j < *count; j++)
        {

            //Get a random starting point between 0-BattleShipBoardSize
            int startingPointX = rand_interval(0, MineSweeperBoardSize - 1);     // 0-board size
            int startingPointY = rand_interval(0, MineSweeperBoardSize);     // 0-board size

            //fprintf(stderr, "starting x=%d\tstarting y=%d\n", startingPointX, startingPointY);

            //fprintf(stderr, "tempOrientation=%d\n\n", tempOrientation);
            //Verify it's a valid point

            while (!isValidPlacement(&population[i], j, startingPointX, startingPointY))
            {
                startingPointX = rand_interval(0, MineSweeperBoardSize - 1);
                startingPointY = rand_interval(0, MineSweeperBoardSize);
            }
            init_Mine(&population[i][j], startingPointX, startingPointY);
            ///fprintf(stderr, "element.(%d) x: %d y=%d\n", j, startingPointX, startingPointY);
            //fprintf(stderr, "Average of game config %d: %f\n", i, AverageMoves[i]);

        }
    }

    int generation, k;
    double maxNum = 0;
    int maxIndex = 0;
    struct Mine children[populationSize][*count];

    /*
    for (k = 0; k < populationSize; ++k)
    {
        // fprintf(stderr, "Game %d\n", k);
        double num = GetAverageMineSweeperPlays(population[k], count, MineSweeperBoardSize);
        //int num=playMineSweeper(&population[0], *count, MineSweeperBoardSize);
        if (num > maxNum)
        {
            maxIndex = k;
            maxNum = num;
        }
        //  fprintf(stderr, "%d %d\n", k, num);


    }
    fprintf(stderr, "The Best Game[%d] took %f moves.\n", maxIndex, maxNum);
*/
    //push_Mine_array_to_json(buffer, &population[0], count, MineSweeperBoardSize, i);
    //fprintf(stderr, "sending %s\n", buffer);
    //  push(buffer);
    //sleep(1);




    /*  BEGIN GENERATIONS   */

    for (generation = 0; generation < generations; generation++)
    {
        //Got our random board!

        //Now, we need to iterate through each element of the population and determine a fitness function

        /*   FITNESS FUNCTION - Play games, randomly picking spots. Average the number of plays for each game for each element in the population. 1-avgMoves / sumAvgMoves = Fitness Ratio   */

        double sumMoves = 0, sumTotalAverages = 0;
        double sumProbabilities = 0.0;
        double probability[populationSize];
        double AverageMoves[populationSize];

        for (j = 0; j < populationSize; j++)
        {

            AverageMoves[j] = GetAverageMineSweeperPlays(population[j], count, MineSweeperBoardSize);
            sumTotalAverages += AverageMoves[j];
            sumMoves = 0;
        }

        for (j = 0; j < populationSize; j++)
        {
            probability[j] = (AverageMoves[j] / sumTotalAverages);
            fprintf(stderr, "setting probability [%d] Average Moves: %f sum: %f = %f\n", j, AverageMoves[j],
                    sumTotalAverages, probability[j]);

        }


        int numChildren = 0;
        while(numChildren < populationSize){
            /*  ROULETTE SELECTION  */
            //Next, we need to iteratively pick parents via roulette selection and cross over.

            int par1 = -1, par2 = -1;
            while (par1 < 0 || par1 >= populationSize)
            {
                par1 = rouletteSelect(probability, populationSize);

            }
            while (par2 == par1 || par2 < 0 || par2 >= populationSize)
            {
                par2 = rouletteSelect(probability, populationSize);

            }
            //  fprintf(stderr, "parent 1=%d parent 2 = %d\n", par1, par2);


            /*  CROSSOVER   */

            struct Mine *parentOne = &population[par1];
            struct Mine *parentTwo = &population[par2];

            fprintf(stderr, "Chose parent[%d] [%d]\norig\n", par1, par2);
/*
            for(i=0; i<populationSize;i++){
                fprintf(stderr, "parent two [%d]\n", i);
                print_Mine(stderr, &parentTwo[i]);
            }*/


            memset(&children[numChildren], 0, sizeof(struct Mine) * populationSize);
          /*  fprintf(stderr, "%d\n", i);
            for(j=0; j<*count; j++){
                print_Mine(stderr, &children[numChildren][j]);
            }*/

            //print_Mine_array(stderr, &child[0], populationSize);
            int numFound = GetCommonMines(&children[numChildren], parentOne, parentTwo, MineSweeperBoardSize);
          /*  fprintf(stderr, "common\n");
            for(j=0; j<*count; j++){
                print_Mine(stderr, &children[numChildren][j]);
            }
            fprintf(stderr, "\n");*/
            FillInUnusedMines(&children[numChildren], parentTwo, numFound, populationSize);
            //print_Mine_array(stderr, &child[0], populationSize);
    /*        fprintf(stderr, "\nNew Child:\n");
            fprintf(stderr, "%d\n", numChildren);
            for(j=0; j<*count; j++){
                print_Mine(stderr, &children[numChildren][j]);
            }
            fprintf(stderr, "\n\n");*/
            numChildren++;

        }
    }







}

/*
    //print_Ship_array(stderr, parentOne, count);
        char buffer[9000];

        for (i = 0; i < populationSize; i++)
        {
            fprintf(stderr, "element.(%d) probability: %f total = %f\n", i, probability[i], sumTotalAverages);
            //fprintf(stderr, "Average of game config %d: %f\n", i, AverageMoves[i]);
            push_Ship_array_to_json(buffer, &population[i], count, BattleShipBoardSize, i);
            push(buffer);
            sleep(2);
        }



    }
   // sleep(10);
}



*/





