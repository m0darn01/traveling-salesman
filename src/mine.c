//
// Created by matthew on 11/16/15.
//
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <math.h>
#include "calculation.h"

#ifdef _WIN32
#include <inttypes.h>
#endif

#include "calculation.h"
#include "mine.h"


int minecmp(struct Mine*one, struct Mine *two, int length)
{
    int i;
    for(i=0; i<length; i++){
        if(one[i].x != two[i].x || one[i].y != two[i].y){
            return -1;
        }
    }
    return 0;
}

void init_Mine(struct Mine *dest, int x, int y)
{
    if(dest == NULL){
        dest = (struct Mine*)malloc(sizeof(struct Mine));
    }
    dest->x = x;
    dest->y = y;
    dest->hit = 0;
}

void copy_Mine(struct Mine **dest, struct Mine **src)
{
    (*dest)->x     = (*src)->x;
    (*dest)->y     = (*src)->y;
    (*dest)->hit = (*src)->hit;
}

void swap_Mine(struct Mine **x, struct Mine **y)
{
    struct Mine temp;
    struct Mine *pTemp = &temp;
    init_Mine(pTemp, (*x)->x, (*x)->y);
    copy_Mine(x, y);
    copy_Mine(y, &pTemp);
}



int isHit(struct Mine* mine, int x, int y)
{
    if(x==mine->x && x==mine->y)
    {
        return 1;
    }
    return 0;
}

int isMine(struct Mine *mines, int length, int x, int y)
{
    int i;
    for (i = 0; i < length; i++)
    {
        //fprintf(stderr, "i=%d %d %d\n",i, mines[i].x, mines[i].y);
        if(x==mines[i].x && y==mines[i].y)
        {
          //  fprintf(stderr, "hit!\n");
            return 1;
        }
    }
    return 0;
}




void setHit(struct Mine *mine)
{
    mine->hit = 1;
}


void clearHit(struct Mine *mine)
{
    mine->hit = 0;
}


void reset(struct Mine *mines, int length)
{
    int i;
    for (i = 0; i < length; i++)
    {
        clearHit(&mines[i]);
    }
}

//Operations
void print_Mine(FILE *stream, struct Mine *mine)
{
    fprintf(stream, "x.(%d)\ty.(%d) .(%s)\n", mine->x, mine->y, mine->hit==1?"hit":"open");
}


void print_Mine_array(FILE *stream, struct Mine *pHead, int *len)
{
        int i;
        for(i=0; i<*len; i++)
            print_Mine(stream, &pHead[i]);
        fprintf(stream, "\n\n");
}

void read_Mine(char *buf, struct Mine *mine)
{
    sprintf(buf, "%s{\"x\":%d,\"y\":%d, \"hit\":\"%s\"}", buf, mine->x, mine->y, mine->hit==1?"hit":"open");
}
void Mine_array_to_json(char *buf, struct Mine *pHead, int *len)
{
    int i;
    strcpy(buf, "{\"resultType\":\"getMines\", \"result\":[");
    char buffer[256];
    for(i=0; i<*len-1; i++){
        strcpy(buffer, "");
        read_Mine(buffer, &pHead[i]);
        strcat(buffer, ", ");
        strcat(buf, buffer);
    }
    read_Mine(buf, &pHead[*len-1]);
    strcat(buf, "]}");
}



void push_Mine_array_to_json(char *buf, struct Mine *pHead, int *len, int generation, int boardSize)
{
    int i;
    sprintf(buf, "{\"resultType\":\"mineLayout\", \"generation\": %d, \"boardSize\": %d, \"result\":[", boardSize, generation);
    char buffer[256];
    for(i=0; i<*len-1; i++){
        strcpy(buffer, "");
        read_Mine(buffer, &pHead[i]);
        strcat(buffer, ", ");
        strcat(buf, buffer);
    }
    strcpy(buffer, "");
    read_Mine(buffer, &pHead[*len-1]);
    strcat(buf, buffer);
    strcat(buf, "]}");
}




















