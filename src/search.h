//
// Created by matthew on 9/14/15.
//

#ifndef TRAVELING_SALESMAN_SEARCH_H
#define TRAVELING_SALESMAN_SEARCH_H

#include "node.h"
//#include "ship.h"
#include "mine.h"
//int breadthFirstSearch(struct Node* cities, uint64_t start, uint64_t goal, uint64_t *step);
void greedy(struct Node *cities, int length, int initialIndex);
void ClosestEdgeInsertionSearch(struct Node *cities, int length, int numStartingRandomValues);
void GeneticAlgorithm(struct Node *cities, uint64_t *count);
//void BattleShipGeneticAlgorithm(struct Ship *ships, int *count);
#endif //TRAVELING_SALESMAN_SEARCH_H
