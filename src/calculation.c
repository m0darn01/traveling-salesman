//
// Created by matthew on 8/28/15.
//

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdint.h>
#include <math.h>

#ifdef _WIN32
#include <inttypes.h>
#endif

#include "calculation.h"

#define DOUBLE 10000000

//Internal Functions
#ifndef _WIN32
void strrev(char *p)
{
    char *q = p;
    while(q && *q) ++q;
    for(--q; p < q; ++p, --q)
        *p = *p ^ *q,
        *q = *p ^ *q,
        *p = *p ^ *q;
}
#endif

//ensure an accuracy of 7 decimal places
void trimUintDigits(uint64_t *x)
{
    if(numDigits(*x) > 7) //should always be the case
    {
        uint64_t i = 0;
        uint64_t numTimesToTrim = numDigits(*x) / 7;
        numTimesToTrim--;
        numTimesToTrim *= 7;
        for (i = 0; i < (numTimesToTrim); i++)
        {
            *x /= 10;
        }
    }
}


void trimIntDigits(int64_t *x)
{
    if(numDigits(*x) > 7) //should always be the case
    {
        uint64_t i = 0;
        uint64_t numTimesToTrim = numDigits(*x) / 7;
        numTimesToTrim--;
        numTimesToTrim *= 7;
        for (i = 0; i < (numTimesToTrim); i++)
        {
            *x /= 10;
        }
    }
}
//End Internal Functions

//portable, deterministic
uint64_t fuint(double x)
{
    char buf[32];
    sprintf(buf, "%0.07f", fabs(x));
    char *pEnd;
    return (uint64_t)(strtod(buf, &pEnd) * DOUBLE);
}

uint64_t numDigits(uint64_t x)
{
    if(x==0)
        return 0;
    return floor(log10(x)) + 1;
}

double uintf(uint64_t x) { return (double)x/DOUBLE; }

//sprintf a uint64_t into a char*, appears as a decimal
void uintcharf(char *buf, uint64_t x)
{
    trimUintDigits(&x);
    if(buf==0){
        fprintf(stderr, "ERROR converting uint to char*. Did you allocate the buffer?\n");
        return;
    }
    strcpy(buf, "");
    char temp[8];
    int i=0;
    while(x!=0){

        if(i++ == 6){
            #ifdef _WIN32
            sprintf(temp, "%" PRIu64 ".", x%10);
            #else
            sprintf(temp, "%lu.", x%10);
            #endif
		}

        else{
            #ifdef _WIN32
            sprintf(temp, "%" PRIu64 "", x%10);
            #else
            sprintf(temp, "%lu", x%10);
            #endif
		}
        strcat(buf, temp);
        x/=10;
    }
    strrev(buf);
}

//get distance from 2 coordinates
uint64_t _distance(uint64_t x1, uint64_t y1, uint64_t x2, uint64_t y2)
{
    uint64_t diffX = x2 > x1 ? x2-x1 : x1-x2;
    uint64_t diffY = y2 > y1 ? y2-y1 : y1-y2;

    uint64_t distance = fuint( sqrt( (diffX*diffX) + (diffY*diffY) )  );
    trimUintDigits(&distance);
    return distance;
}

uint64_t distance(struct Node* x, struct Node* y)
{
    return _distance(x->x, x->y, y->x, y->y);
}




//Compute the dot product AB . AC
double DotProduct(uint64_t Ax,uint64_t Ay,uint64_t Bx,uint64_t By,uint64_t Cx,uint64_t Cy)
{
    double ABx, ABy, BCx, BCy;

    ABx = uintf(Bx) - uintf(Ax);
   // trimIntDigits(&ABx);
    ABy = uintf(By) - uintf(Ay);
   // trimIntDigits(&ABy);
    BCx = uintf(Cx) - uintf(Bx);
  //  trimIntDigits(&BCx);
    BCy = uintf(Cy) - uintf(By);
   // trimIntDigits(&BCy);
    double dot = ABx * BCx + ABy * BCy;
   // trimIntDigits(&dot);
    return dot;
}

//Compute the cross product AB x AC
double CrossProduct(uint64_t Ax,uint64_t Ay,uint64_t Bx,uint64_t By,uint64_t Cx,uint64_t Cy)
{
    double ABx, ABy, ACx, ACy;
    ABx = uintf(Bx) - uintf(Ax);
   // trimIntDigits(&ABx);
    ABy = uintf(By) - uintf(Ay);
    //trimIntDigits(&ABy);
    ACx = uintf(Cx) - uintf(Ax);
    //trimIntDigits(&ACx);
    ACy = uintf(Cy) - uintf(Ay);
    //trimIntDigits(&ACy);
    double cross = (ABx * ACy - ABy * ACx);
    //trimIntDigits(&cross);
    return cross;
}


//Compute the distance from AB to C
uint64_t PointToLineSegment(uint64_t PointX, uint64_t PointY, uint64_t LinePointA_x, uint64_t LinePointA_y, uint64_t LinePointB_x, uint64_t LinePointB_y)
{
    double dist = CrossProduct(LinePointA_x, LinePointA_y, LinePointB_x, LinePointB_y, PointX, PointY) / uintf(_distance(LinePointA_x, LinePointA_y, LinePointB_x, LinePointB_y));
    double dot1 = DotProduct(LinePointA_x, LinePointA_y, LinePointB_x, LinePointB_y, PointX, PointY);
    //trimIntDigits(&dot1);
    if (dot1 > 0) 
        return _distance(LinePointB_x, LinePointB_y, PointX, PointY);

    double dot2 = DotProduct(LinePointB_x, LinePointB_y, LinePointA_x, LinePointA_y, PointX, PointY);
    //trimIntDigits(&dot2);
    if (dot2 > 0) 
        return _distance(LinePointA_x, LinePointA_y, PointX, PointY);
   // trimIntDigits(&dist);
    return fuint(abs(dist));
} 
/*

uint64_t PointToLineSegment(uint64_t PointX, uint64_t PointY, uint64_t LinePointA_x, uint64_t LinePointA_y, uint64_t LinePointB_x, uint64_t LinePointB_y)
{
    uint64_t dx = abs(LinePointB_x-LinePointA_x);
    uint64_t dy = abs(LinePointB_y-LinePointA_y);

    trimUintDigits(&dx);
    trimUintDigits(&dy);
    uint64_t t = abs(((PointX - LinePointA_x) * dx + (PointY - LinePointA_y) * dy) / (dx * dx + dy * dy));

    trimUintDigits(&t);

    dx = PointX - LinePointB_x;
    dy = PointY - LinePointB_y;
    return fuint(sqrt(dx * dx + dy * dy));

}

*/
