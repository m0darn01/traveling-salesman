//
// Created by matthew on 9/20/15.
//
#include <stdio.h>
#include <string.h>
#include <assert.h>
#include <nanomsg/src/nn.h>
#include <nanomsg/src/pair.h>
#include "queue.h"
#include "thread.h"
#include "comm/api.h"
#include "comm/nanomsg.h"


static int sock;

const char *url = "ipc:///tmp/pair3.ipc";

pthread_t sendThread,recvThread;
pthread_mutex_t commLock;
char outBuffer[8192], inBuffer[2048];

queue_t outQ;

static int sendFlag;

/*
 * Send Handler
 */
int SendHandler()
{
    char *jsonstr = NULL;
    int quit = 0;
    int sz_n;
    while(quit == 0){
        if ( (jsonstr= queue_dequeue(&outQ, 1)) != 0 )
        {
            if(strcmp(jsonstr, "quit")==0){
                quit=1;
                free_queueitem(jsonstr);
                break;
            }
            pthread_mutex_lock(&commLock);
            sz_n = strlen (jsonstr);// + 1; // '\0' too
            nn_send (sock, jsonstr, sz_n, 0);
            pthread_mutex_unlock(&commLock);
            free_queueitem(jsonstr);
        }
    }
    pthread_exit(0);
}
/*
 * Receive Handler
 */
int ReceiveHandler()
{
    int quit=0;
    while(quit == 0){
        if(strcmp(inBuffer, "quit") ==0){
            quit = 1;
            break;
        }
        if(pthread_mutex_lock(&commLock) != 0){
            continue;
        }
        char *msg = NULL;
        int receive = recv(&msg);
        pthread_mutex_unlock(&commLock);
        if(receive > 0){
            //success
            api(msg);
            free(msg);
        }
        else{
            //didn't receive anything
        }
        usleep(500);
    }
    pthread_exit(0);

}


void init_threads()
{
    sendFlag = 0;
    pthread_mutex_init(&commLock, NULL);
    int to=3;
    sock = nn_socket (AF_SP, NN_PAIR);
    assert (sock >= 0);
    assert (nn_setsockopt (sock, NN_SOL_SOCKET, NN_RCVTIMEO, &to, sizeof (to)) >= 0);
    assert (nn_bind (sock, url) >= 0);
    pthread_create (&sendThread, NULL, &SendHandler, NULL);
   // sleep(1);
    pthread_create (&recvThread, NULL, &ReceiveHandler, NULL);
   // sleep(1);
    fprintf(stderr, "Server started at %s\n", url);
}

void stop_threads()
{
    pthread_mutex_lock(&commLock);
    strcpy(outBuffer, "quit");
    strcpy(inBuffer, "quit");
    pthread_mutex_unlock(&commLock);
    sleep(1);
    pthread_mutex_destroy(&commLock);
    shutdown_nn();
    fprintf(stderr, "Stopping Threads\n");
}

int push(const char* msg)
{
    queue_enqueue("outQ", &outQ, queueitem(msg));
    return 0;
}